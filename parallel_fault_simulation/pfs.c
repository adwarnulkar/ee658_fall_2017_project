/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

// PARALLEL FAULT SIMULATION

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "pfs.h"

int main(int argc, char *argv[])
{
    strcpy(ckt_name,argv[1]);
    strcpy(input_vector_file,argv[2]);
    strcpy(fault_list_file,argv[3]);
    strcpy(output_vector_file,argv[4]);

    reg_size = 64;
    head = NULL;
	
	//final_fault_list = NULL;
	final_fault_list_length = 0;
    cread(ckt_name);
    lev();
    fault_read();

	struct node* temp = head;
	while (temp!=NULL) {
        final_fault_list_length++;      
		temp = temp->next;
    }

	float percent = (final_fault_list_length / (1.0 * no_faults)) * 100.0;
	printf("\nFAULT COVERAGE = %f percent\n", percent);

    clear();
    return 0;
}

