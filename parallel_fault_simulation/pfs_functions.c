/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

/* This file implements following functions
 * levelization
 * simple_logic_simulator
 * fault_read
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h> 
#include <math.h>

#include "pfs.h"

/* Function : levelization 
 * Description : Parse through all the nodes and evaluated level of each node
 * input argument : none
 */
void lev ()
{
	int i,j,all_ini,max;
	
	NSTRUC *np;
	for (i=0;i<Npi;i++)
		Pinput[i]->level = 0;
	all_ini=Nnodes-Npi;
	while (all_ini > 0)
	{
		for(i = 0; i<Nnodes; i++)
		{
			np = &Node[i];
			if (np->type != IPT){
				int count = 0; int arr[np->fin];
				memset(arr,-1,np->fin);
				for(j = 0; j<np->fin; j++)
				{	
					if (np->unodes[j]->level == -1) break;
					arr[j]=np->unodes[j]->level;
					count++;	
				}
				max = arr[0];
				if (count == np->fin)
				{
					for(j = 0; j<np->fin; j++){
						if (arr[j] > max)
							max = arr[j];
					}
					if (np->level == -1)
						all_ini--;
					np->level=max+1;
					//all_ini--;	
				}	
			}
		}
	}
	
}

/* Function : simple_logic_simulator
 * Description : Performs parallel logic simulation
 * input argument : File handle to read input vectors
 */
void simple_logic_simulator(FILE* fd3)
{
	
	int i,l,k,h,max_level;
	l=1;
	NSTRUC *np;
	
    max_level=0;
	for(i = 0; i<Nnodes; i++)
	{	np = &Node[i];
		if (np->level > max_level)
		max_level=np->level;
	}		
	
		
		for (l=0;l <= max_level;l++){		
			for(i = 0; i<Nnodes; i++)
			{
				np = &Node[i];
				if (np->level == l){  
							if(np->type == BRCH) {
								for (h=0;h<np->fin;h++)
								np->value = np->unodes[h]->value;
								
							}
							else if(np->type == XOR) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
								np->value = (np->value) ^ (np->unodes[h]->value);
							}
							else if(np->type == OR) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
									np->value = (np->value) | (np->unodes[h]->value);
							}
							else if(np->type == NOR) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=0;h<np->fin;h++)
									np->value = (((np->value) | (np->unodes[h]->value)));
								np->value = (~(np->value));
							}
							else if(np->type == NOT) {
								h=0;
								np->value = np->unodes[h]->value;
								np->value = (~(np->value));
							}
							else if(np->type == NAND) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
									np->value = (np->value) & (np->unodes[h]->value);
								np->value = (~(np->value));
							}
							else if(np->type == AND){
								h=0;						
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
									np->value = (np->value) & (np->unodes[h]->value);
							}
								np->value = (((np->and_mask) & np->value) | (np->or_mask));
				}	  
			}			
	    }

		for (k=0;k<Npo;k++){
			i = 1;
			if ((Poutput[k]->value)%2 == 0)
				last_bit_repeated_64_times = 0;
			else 
				last_bit_repeated_64_times = -1;
			
			compare_result = last_bit_repeated_64_times ^ Poutput[k]->value;
			compare_result = compare_result >> 1;
			while ((i < reg_size) && (fault_array[i] != -1))
			{
				if (compare_result%2 != 0){
					
					if (find(fault_array[i]) == 0)
					{
						struct node* temp = (struct node*)malloc(sizeof(struct node));
						temp->final_fault_list = fault_array[i];
						temp->next = head;
						head = temp;
					}
					
				}
				i++;
				compare_result = compare_result >> 1;	
			}
		}

	   
}

/* Function : fault_read
 * Description : Read the fault from fault list and sets the AND/OR MASK accordingly
 * input argument : none
 */
void fault_read(){
	FILE *fd3;
	fd3 = fopen(output_vector_file,"w");
	int debug_count = 0;
	FILE *fd1, *fd2; 
	char c[MAXLINE],c1[MAXLINE];

	if((fd1 = fopen(input_vector_file,"r")) == NULL) printf("input vector file does not exist" );
	
	NSTRUC *np;
	long long int a = 1;
	int j=1;
    int i=0;
	memset(fault_array,0,reg_size);
				
	if((fd2 = fopen(fault_list_file,"r")) == NULL) printf("fault file does not exist" );

	float loop_count = 0;
	no_faults = 0;

	while ((fgets(c1,MAXLINE,fd2)) != NULL)
		no_faults++;

	fclose(fd2);

	loop_count = ((float)no_faults)/(reg_size - 1);
	int loop_count_int = (int)ceil(loop_count);
	int t;

	while ((fgets(c,MAXLINE,fd1)) != NULL) //reading input vectors
	{
        if (c[0] != '-')
        {
            fd2 = fopen(fault_list_file,"r");

		    for (t=0; t < loop_count_int; t++)
		    {
		    	
		    	for (i=0;i<reg_size;i++)	//initialising fault array of 64 elements to 1
		    		fault_array[i] = -1;
		    		
		    	
		    	for (i=0;i<Npi;i++){
		    		Pinput[i]->value = c[i] - '0';
		    		if (Pinput[i]->value == 1)
		    			Pinput[i]->value = -1;
		    	}
		    	
		    	//populating fault list
		    	j=1;
		    	while((j < reg_size ) && (fscanf(fd2,"%d", &fault_array[j]) != EOF)) {
		    		j++;
		    	}
		    	reset_mask();
		    	
		    	//mask setter	
		    	for (i=1;i<reg_size;i++){
		    		if (fault_array[i] != -1) {
		    			int index = (fault_array[i])/10;
		    			int fault_type = (fault_array[i])%10;
		    			np = &Node[index];
		    			if (fault_type == 1)	//stuck at 1 fault
		    				np->or_mask = a << i;
		    			else
		    				np->and_mask = np->and_mask & (~(a << i));
		    		}
		    	}
		    	
		    	debug_count++;
		    	simple_logic_simulator(fd3);
		    }

	        fclose(fd2);
        }
	}
	fclose(fd1);

    // Print the results in output file
	if((fd1 = fopen(input_vector_file,"r")) == NULL) printf("input vector file does not exist" );
	if((fd2 = fopen(fault_list_file,"r")) == NULL) printf("fault file does not exist" );
    int fault_flag;
    int fault_code;
	
    while ((fgets(c,MAXLINE,fd1)) != NULL) //reading input vectors
    {
        //Read the fault code
        fgets(c1,MAXLINE,fd2);
		fault_code = atoi(c1);
        fault_flag = 0;

        if (c[0] != '-')
		{
			struct node* temp = head;
			while (temp!=NULL) {

				if(fault_code == temp->final_fault_list)
                {
                    fault_flag = 1;
                    break;
                }
				temp = temp->next;
            }   

            if (fault_flag == 1)
            {
                fprintf(fd3, "FAULT_DETECTED ;; Fault is %4d ;; test_vector is %s",fault_code,c);
            }
            else
            {
                fprintf(fd3, "FAULT_NOT_DETECTED ;; Fault is %4d ;; test_vector is %s",fault_code,c);
            }
        }
        else
        {
            fprintf(fd3, "FAULT_NOT_DETECTED ;; Fault is %4d ;; test_vector is ---\n",fault_code);
        }
    }
		
	fclose(fd1);
	fclose(fd2);
    fclose(fd3);


	
}

/* Function : reset_mask
 * Description : Reset AND/OR mask after each pass of PFS
 * input argument : none
 */
void reset_mask(){
NSTRUC *np;
int i;
	for (i=0;i<Nnodes;i++){
		np = &Node[i];
		np->or_mask = 0;
		np->and_mask = -1;
	}
}

/* Function : find
 * Description : find a value in the linked list
 *               Linked list is used to store the union of all the faults that are detected
 * input argument : value
 */
int find(int value)
{
	struct node* temp = head;
	while (temp!=NULL)
	{
		if(value == temp->final_fault_list)
			return 1;
		temp = temp->next;
	}
		return 0;
}
