# README #

## Command to run overall system (collapsed fault list)
#### Syntax
./run_script_demo.sh <ckt name> <atpg algo> <fault simulator algo>

#### Example
./run_script_demo.sh c17 podem dfs

#### Possible circuit names
- c17
- c880
- c1355

#### Possible ATPG algo names
- d_algo
- podem

#### Possible Fault Simulator names
- dfs
- pfs

**Note** : D-Algo is NOT efficient algorithm for c1355

## Command to run overall system (full fault testing)
#### Syntax
./run_script_exhaustive.sh <ckt name> <atpg algo> <fault simulator algo>

#### Example
./run_script_exhaustive.sh c17 podem dfs

## Log Paths
- logs_demo : contains log paths for demo runs. The Folder name corresponds to the data and time at which the script was
- logs_exhaustive : contains log paths for full fault list testing. The Folder name corresponds to the data and time at which the script was run.

Note : 
- Soft link **latest** always points the latest run of the script.
- session.log : Contains the arguments passed to the script.


## Directory structure
- circuits : Contains ckt files
- golden_files : Contains golden fault list , test vectors , outputs for benchmark circuits
- pre_preprocessor : Contains JAVA implementation of fault collapsing and d_algo
- podem : Contains C implementation of PODEM algorithm
- deductive_fault_simulation : Contains C implementation of deductive fault simulation
- parallel_fault_simulation : Contains C implementation of parallel fault simulation
- simple_logic_simulation : Contains C implementation of simple logic simulation
- reports : Contains flow chart and pseudo code reports for each phase of the project
- scripts : Contains Python scripts used to generate exhaustive vectors and fault list
- python_notebooks : Contains Python Jupyter notebooks for logic simulation , podem and dfs
- python_implementation : Contains Python Implementation for atpg verification and podem
- Images : Contains images of flowcharts and performance plots


## Steps to run preprocessor
- cd pre_processor
- make clean
- make
- java PreProcessor <ckt file path> <collapsed fault list path>


## Steps to run D-algo
- cd pre_processor
- make clean
- make
- java DAlgoTop <ckt file path> <fault list path> <test vector path>


## Steps to run PODEM 
- cd podem
- make clean
- make
- make <circuit name>_pp
- Example : make c17_pp


## Steps to run DFS
- cd deductive_fault_simulation
- make clean
- make
- make <circuit name>_pp
- Example : make c17_pp


## Steps to run PFS
- cd parallel_fault_simulation
- make clean
- make
- make <circuit name>_pp
- Example : make c17_pp


## Team Members
University of Southern California , Fall 2017

- Divya Nandihalli (divyarn29@gmail.com)
- Deven Gupta (devengupta0406@gmail.com)
- Aditya Warnulkar (adwarnulkar@gmail.com)
- Sayatan Ghosh (sayantag@usc.edu)
