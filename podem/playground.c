/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>       
#include <time.h>

#include "podem.h"
 
void pc(); 
char *gname(int);
 
int main(int argc, char *argv[])	//argv[1] = .ckt file argv[2] = input vector file argv[3] = output file name
{

	int podem_result, i,k;
	char c[MAXLINE];
    strcpy(ckt_name,argv[1]);
	strcpy(input_vector_file,argv[2]);
	strcpy(output_vector_file,argv[3]);
	
	//Create Node Data structures
	cread(ckt_name);
	lev();
	
	//reading from file
	FILE *fd1,*fd2;
	if((fd1 = fopen(input_vector_file,"r")) == NULL)
      		printf("input vector file for logic simulation does not exist" );
	fd2 = fopen(output_vector_file,"w");
	
	//Time keeper
	clock_t start, end;
    double cpu_time_used;
	
	//Run Podem for all the faults in fault list file
	while ((fgets(c,MAXLINE,fd1)) != NULL)
	{
		for (k=0; k<Nnodes; k++)
		{
			Node[k].value = 4;
		}
		
		podem_count = 0;	
		head = NULL;
		
		//initialising faults
		fault_code = atoi(c);
		fault_idx = fault_code / 10;
		fault_type = fault_code % 10;
		
		//Execute Podem
		start = clock();
		podem_result = podem();
		end = clock();
		
		cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
		
		if (podem_result == 1)
		{
			//fprintf(fd2,"Fault is %d ; Test Vector is ",fault_code);
			for (i=0; i<Npi; i++){
				if (Pinput[i]->value == 2)
					fprintf(fd2,"1");
				else if (Pinput[i]->value == 3)
					fprintf(fd2,"0");
				else if (Pinput[i]->value == 4)
					fprintf(fd2,"x");
				else
					fprintf(fd2,"%d",Pinput[i]->value);
			}
		}
		else
		{
			for (i=0; i<Npi; i++)
			{
				frpintf(fd2,"-");
			}
		}
		fprintf(fd2,"\n");
		
		//Print time elapsed
		printf("Time elapsed for fault %d is %lf\n",fault_code,cpu_time_used);
	}
	clear();
	fclose(fd1);	
	fclose(fd2);
    return 0;
} 
 
void pc()
{
   int i, j;
   NSTRUC *np;
   char *gname();

   printf("\nPrinting Circuit Description\n");
   printf("\n Node   Type \tIn     \t\t\tOut    \t\tLEVEL\t\tVALUE\t\tINDEX\n");
   printf("------ ------\t-------\t\t\t-------\t\t-----\t\t-----\t\t-----\n");

   for(i = 0; i<Nnodes; i++) {
      np = &Node[i];
      printf("\r%4d  %6s\t", np->num, gname(np->type));
      for(j = 0; j<np->fin; j++) printf("%d ",np->unodes[j]->num);
      printf("\t\t\t");
      for(j = 0; j<np->fout; j++) printf("%d ",np->dnodes[j]->num);
      printf("\t\t");
      printf("%d", np->level);
	  printf("\t\t");
	  printf("%d", np->value);
	  printf("\t\t");
	  printf("%d", np->indx);
	  printf("\n");
   }

   printf("\nPrimary inputs:  ");
   for(i = 0; i<Npi; i++) printf("%d ",Pinput[i]->num);
   printf("\n");
   printf("Primary outputs: ");
   for(i = 0; i<Npo; i++) printf("%d ",Poutput[i]->num);
   printf("\n\n");
   printf("Number of nodes = %d\n", Nnodes);
   printf("Number of primary inputs = %d\n", Npi);
   printf("Number of primary outputs = %d\n", Npo);
  
}

char *gname(int tp)

{
   switch(tp) {
      case 0: return("PI");
      case 1: return("BRANCH");
      case 2: return("XOR");
      case 3: return("OR");
      case 4: return("NOR");
      case 5: return("NOT");
      case 6: return("NAND");
      case 7: return("AND");
   }
}
