/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>       
#include "podem.h"

/* Function : error_at_PO 
 * Description : Check if D/D_bar exists at PO
 * input argument : none
 */
int error_at_PO()
{

	int i, count = 0;
	for (i=0; i<Npo; i++)
	{
		if ((Poutput[i]->value == 2) || (Poutput[i]->value == 3))
			return SUCCESS;
		count++;
	}
	
	if (count == Npo)
		return FAILURE;

}
