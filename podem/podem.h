#ifndef __PODEM_FUNCTIONS__
#define __PODEM_FUNCTIONS__

extern int cread();

extern void lev();

extern void simple_logic_simulator();

extern int error_at_PO();

extern int test_not_found();

extern void imply(int, int);

extern void clear(); 

extern void allocate(); 

extern int podem(); 

extern void print_result(FILE*); 

extern int *backtrace(int, int);

extern int *objective();

extern void put_d_frontier();



enum e_ntype {GATE, PI, FB, PO};    /* column 1 of circuit format */
enum e_gtype {IPT, BRCH, XOR, OR, NOR, NOT, NAND, AND};  /* gate types */

#define MAXLINE 500               /* Input buffer size */

#define MAXNAME 500               /* File name size */

#define SUCCESS 1

#define FAILURE 0

char input_vector_file[MAXNAME];

char output_vector_file[MAXNAME];

char ckt_name [MAXNAME];

typedef struct n_struc {
   unsigned indx;             /* node index(from 0 to NumOfLine - 1 */
   unsigned num;              /* line number(May be different from indx */
   enum e_gtype type;         /* gate type */
   unsigned fin;              /* number of fanins */
   unsigned fout;             /* number of fanouts */
   struct n_struc **unodes;   /* pointer to array of up nodes */
   struct n_struc **dnodes;   /* pointer to array of down nodes */
   int level;                 /* level of the gate output */
   int value;
   int ip;						//inversion parity
   int c;						//controlling
     
} NSTRUC;

struct node {
	int data;
	struct node* next;
};

struct node* head;
//head = NULL; doing this in main

/*------------------------------------------------------------------------*/
NSTRUC *Node;                   /* dynamic array of nodes */
NSTRUC **Pinput;                /* pointer to array of primary inputs */
NSTRUC **Poutput;               /* pointer to array of primary outputs */
int Nnodes;                     /* number of nodes */
int Npi;                        /* number of primary inputs */
int Npo;                        /* number of primary outputs */
/*------------------------------------------------------------------------*/

int fault_idx;
int fault_type;		
int fault_code;	
int *imply_array;
int *obj_array;
int podem_count;
int test_np_count;
int try_reverse;

extern void and(NSTRUC*);

extern void or(NSTRUC*);

extern void xor(NSTRUC*);

extern void not(NSTRUC*);

extern void not_2(NSTRUC *np);

extern void insert_d_frontier(NSTRUC*);

#endif
