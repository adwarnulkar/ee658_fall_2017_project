/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "podem.h"

/* Function : imply 
 * Description : Assigns node value to node index and performs 5 valued logic simulation
 * input argument : node index , node value
 */
void imply(int idx, int val)
{
	if (idx != fault_idx)
		Node[idx].value = val;
	
    simple_logic_simulator();
	
}

/* Function : simple_logic_simulator 
 * Description : Performs 5 valued logic simulation
 * input argument : none
 */
void simple_logic_simulator()
{
	int i,l;
	int max_level;
	l=1;
	NSTRUC *np;
	max_level=0;
	for(i = 0; i<Nnodes; i++)
	{	np = &Node[i];
		if (np->level > max_level)
		max_level=np->level;
	}		
			
		for (l=0;l <= max_level;l++){		
			for(i = 0; i<Nnodes; i++)
			{
				np = &Node[i];
				if (np->level == l)
				{	
							if(np->type == BRCH) {
									np->value = np->unodes[0]->value;
							}
							
							else if(np->type == XOR) {																							
								xor(&Node[i]);								
							}
							
							else if(np->type == OR) {																		
								or(&Node[i]);								
							}
							
							else if(np->type == NOR) {								
								or(&Node[i]);
								not_2(&Node[i]);								
							}
							
							else if(np->type == NOT) {									
								not(&Node[i]);								
							}
							
							else if(np->type == NAND) {
								and(&Node[i]);
								not_2(&Node[i]);								
							}
							
							else if(np->type == AND){													
								and(&Node[i]);						
							}	
					if (np->indx == fault_idx)
					{
						if (np->value == !(fault_type))
						{
							if (fault_type == 0) //SA0
							{
								np->value = 2; //D
							}
							else //SA1
							{
								np->value = 3; //D_bar
							}
								
						}
					}		
				}
				
			}			
	    }
}

/* Function : backtrace 
 * Description : Find the node index and value of PI which results in value vk on line k
 * input argument : node index k , node value vk
 * return argument : PI node index , PI node value
 */
int *backtrace(int k, int vk)
{
	imply_array = (int*)malloc(2*sizeof(int));
	int v,i,j; //i is inversion parity of gate k
	v = vk;
	while(Node[k].type != IPT)
	{
		
		if (Node[k].type == XOR)
		{
			//to be done
			printf("XOR not yet implemented\n");
		}
		else
		{	
			i = Node[k].ip;
            if (try_reverse == 0)
            {
			    for (j=0; j<Node[k].fin; j++)
			    {
			    	if (Node[k].unodes[j]->value == 4)
			    	{
			    		k = Node[k].unodes[j]->indx;
			    		v = v ^ i;
			    		break;
			    	}
			    }
            }
            else
            {
			    for (j=Node[k].fin-1; j>=0; j--)
			    {
			    	if (Node[k].unodes[j]->value == 4)
			    	{
			    		k = Node[k].unodes[j]->indx;
			    		v = v ^ i;
			    		break;
			    	}
			    }
            }
			
		}
	}
	
    imply_array[0] = k;
	imply_array[1] = v;
	
    return imply_array;
}

/* Function : objective
 * Description : objectives are selected so that target fault is activated , 
 *               then the resulting error is propogated to PO
 * input argument : none
 * return argument : node index , node value
 */
int* objective()
{
	int i;
	obj_array = (int*)malloc(2*sizeof(int));

    // Set fault line L s-a-v to value v_bar
	if (Node[fault_idx].value == 4)
	{
		obj_array[0] = fault_idx;
		if (fault_type == 0)
		{
			Node[fault_idx].value = 2;
			obj_array[1] = 1;
		}
		else
		{
			Node[fault_idx].value = 3;
			obj_array[1] = 0;
		}
		return obj_array;
	}

	// Pick gate from D-Frontier and assign value c_bar to its input
    for (i=0; i<Node[head->data].fin; i++)
	{
		if (Node[head->data].unodes[i]->value == 4)
		{
			obj_array[0] = Node[head->data].unodes[i]->indx;
			if (Node[head->data].c == 0){
			obj_array[1] = 1;
			}
			else{
				
				obj_array[1] = 0;
			}
			return obj_array;					
		}
		
	}
			
}

/* Function : put_d_frontier
 * Description : Parse through all the nodes and find the D-Frontiers
 *               D-Frontier has been implemented as linked list
 * input argument : none
 * return argument : none
 */
void put_d_frontier()
{
	struct node* temp;
	while (head != NULL)
    { 
		temp = head; 
        head = head->next;
        free(temp);
    }
	
	int i,j;
	for (i=0; i<Nnodes; i++)
	{
		if ((Node[i].type != IPT) && (Node[i].type != BRCH) && (Node[i].type != NOT) && (Node[i].value == 4))
		{
			for (j=0; j<Node[i].fin; j++)
			{
				if ((Node[i].unodes[j]->value == 2) || (Node[i].unodes[j]->value == 3))
				{
					insert_d_frontier(&Node[i]);
					break;
				}
			}
		}
	}
}

/* Function : insert_d_frontier
 * Description : Insert node index to D-Frontier linked list
 * input argument : struct pointer
 */
void insert_d_frontier(NSTRUC *np)
{
	struct node* temp = (struct node*)malloc(sizeof(struct node));
	struct node* current;
	
	temp->data = np->indx;
	temp->next = NULL;
	
	if(head == NULL){
		head = temp;
		current = temp;
	}
	else
	{		
			current = head;
			while(current->next != NULL){
				current = current->next;
			}
		current->next = temp;
	}
	
}
