/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>       
#include "podem.h"

/* Function : test_not_found 
 * Description : Check if test is possible , by checking following conditions
 *               1) Does fault line L have value v
 *               2) Is the D-Frontier empty
 *               3) Is the fault line L , x 
 *               4) Have all the possible input combinations tried ?
 * input argument : none
 */
int test_not_found()
{
	int condition_1,condition_2,condition_3,condition_4;

	//check if line L has value v
	if(Node[fault_idx].value == fault_type)
	{
		condition_1 = 1;
	}
	else
	{
		condition_1 = 0;
	}
	
	//checking if D frontier is empty or no
	if(head == NULL)
	{
		condition_2 = 1;
	}
	else
	{
		condition_2 = 0;
	}
	
	//check if line L is x
	if(Node[fault_idx].value == 4)
	{
		condition_3 = 1;
	}
	else
	{
		condition_3 = 0;
	}

    //check if enough implications have been tried
    if(test_np_count > 2*Npi)
    {
        condition_4 = 1;
    }
    else
    {
        condition_4 = 0;
    }
	
	//returning criteria
	if(condition_1 || condition_4 || (condition_2 && !(condition_3)))
	{
		test_np_count++;
        return SUCCESS;
	}
	else
	{
		return FAILURE;
	}
	
}
