/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "podem.h"

/* Function : podem 
 * Description : Recursive function which implements podem algorithm
 * input argument : none
 */
int podem()
{
	int *obj_in_podem, *bkt_in_podem;
	podem_count++;
	
	if (error_at_PO())
		return SUCCESS;
	
	if (test_not_found())
		return FAILURE;
	
	obj_in_podem = objective();
	
	bkt_in_podem = backtrace(obj_in_podem[0],obj_in_podem[1]);
	
	imply(bkt_in_podem[0],bkt_in_podem[1]);
	put_d_frontier();
	
	if (podem() == SUCCESS)
		return SUCCESS;
	
	//reverse decisiom
	imply(bkt_in_podem[0],!(bkt_in_podem[1]));
	put_d_frontier();
	
	if (podem() == SUCCESS)
		return SUCCESS;
	
	imply(bkt_in_podem[0],4);
	put_d_frontier();
	return FAILURE;

}

/* Function : print_result
 * Description : prints the results of podem into a vector file
 * input argument : output vector file handle
 */
void print_result(FILE* fd2)
{

    int i;

    for (i=0; i<Npi; i++){
    	if (Pinput[i]->value == 2)
    		fprintf(fd2,"1");
    	else if (Pinput[i]->value == 3)
    		fprintf(fd2,"0");
    	else if (Pinput[i]->value == 4)
			fprintf(fd2,"1");
    	else
    		fprintf(fd2,"%d",Pinput[i]->value);
    }
    
}
