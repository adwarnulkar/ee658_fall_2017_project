/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>       
#include <time.h>

#include "podem.h"
 
void pc(); 
char *gname(int);
 
int main(int argc, char *argv[])
{
	/* Command line arguments
    argv[1] = .ckt file
    argv[2] = input vector file
    argv[3] = output file name
    */

    int podem_result;
    int podem_forward_result,podem_backward_result;
    int k,i;
    double total_podem_time = 0;

	char c[MAXLINE];
    strcpy(ckt_name,argv[1]);
	strcpy(input_vector_file,argv[2]);
	strcpy(output_vector_file,argv[3]);
	
	//Create Node Data structures
	cread(ckt_name);
	lev();
	
	//reading fault list and output vector file
	FILE *fd1,*fd2;
	if((fd1 = fopen(input_vector_file,"r")) == NULL)
      		printf("input vector file for logic simulation does not exist" );
	fd2 = fopen(output_vector_file,"w");
	
	//Time keeper
	clock_t start, end;
    double cpu_time_used;
	
	printf("Number of PI : %d\n",Npi);
    
    //Run Podem for all the faults in fault list file
	while ((fgets(c,MAXLINE,fd1)) != NULL)
	{
		//initialising faults
		fault_code = atoi(c);
		fault_idx = fault_code / 10;
		fault_type = fault_code % 10;

        printf("Processing fault : %d\n",fault_code);

        //Reset node values to 'x'
		for (k=0; k<Nnodes; k++)
		{
			Node[k].value = 4;
		}
		
		//Empty the D-Frontier
        head = NULL;

        //Rest podem variables
        test_np_count = 0;
        try_reverse = 0;
		podem_count = 0;	
        podem_forward_result = 0;
        podem_backward_result = 0;

		//Execute Podem
		start = clock();
        podem_forward_result = podem();

        if (podem_forward_result == 0)
        {
            try_reverse = 1;

            //Reset the nodes to 'x'
		    for (k=0; k<Nnodes; k++)
		    {
		    	Node[k].value = 4;
		    }

            podem_count = 0;
            test_np_count = 0;
            head = NULL;

            podem_backward_result = podem();

        }
		end = clock();
		
		// Evaluate time taken by podem algo per fault
        cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
        total_podem_time = total_podem_time + cpu_time_used;

        podem_result = podem_forward_result || podem_backward_result;
		
		if (podem_result == 1)
		{
		    print_result(fd2);	
		}
		else
		{
			for (i=0; i<Npi; i++)
			{
				fprintf(fd2,"-");
			}
		}
		
        fprintf(fd2,"\n");
		
	}
	clear();
	fclose(fd1);	
	fclose(fd2);

    printf("Total time elapsed : %0.4lf seconds\n",total_podem_time);

    return 0;
} 
 
void pc()
{
   int i, j;
   NSTRUC *np;
   char *gname();

   printf("\nPrinting Circuit Description\n");
   printf("\n Node   Type \tIn     \t\t\tOut    \t\tLEVEL\t\tVALUE\t\tINDEX\n");
   printf("------ ------\t-------\t\t\t-------\t\t-----\t\t-----\t\t-----\n");

   for(i = 0; i<Nnodes; i++) {
      np = &Node[i];
      printf("\r%4d  %6s\t", np->num, gname(np->type));
      for(j = 0; j<np->fin; j++) printf("%d ",np->unodes[j]->num);
      printf("\t\t\t");
      for(j = 0; j<np->fout; j++) printf("%d ",np->dnodes[j]->num);
      printf("\t\t");
      printf("%d", np->level);
	  printf("\t\t");
	  printf("%d", np->value);
	  printf("\t\t");
	  printf("%d", np->indx);
	  printf("\n");
   }

   printf("\nPrimary inputs:  ");
   for(i = 0; i<Npi; i++) printf("%d ",Pinput[i]->num);
   printf("\n");
   printf("Primary outputs: ");
   for(i = 0; i<Npo; i++) printf("%d ",Poutput[i]->num);
   printf("\n\n");
   printf("Number of nodes = %d\n", Nnodes);
   printf("Number of primary inputs = %d\n", Npi);
   printf("Number of primary outputs = %d\n", Npo);
  
}

char *gname(int tp)
{
   switch(tp) {
      case 0: return("PI");
      case 1: return("BRANCH");
      case 2: return("XOR");
      case 3: return("OR");
      case 4: return("NOR");
      case 5: return("NOT");
      case 6: return("NAND");
      case 7: return("AND");
   }
}
