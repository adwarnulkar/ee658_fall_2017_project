#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

//#include "podem.h"
#include "gate.h"

void and(NSTRUC *np)
{
	
	int h;
	np->value = np->unodes[0]->value;
	for (h=1;h<np->fin;h++){
		np->value = table_and[np->unodes[h]->value][np->value];		
	}
	
}

void or(NSTRUC *np)
{
	
	int h;
	np->value = np->unodes[0]->value;
	for (h=1;h<np->fin;h++){
		np->value = table_or[np->unodes[h]->value][np->value];		
	}
}

void xor(NSTRUC *np)
{
	
	int h;
	np->value = np->unodes[0]->value;
	for (h=1;h<np->fin;h++){
		np->value = table_xor[np->unodes[h]->value][np->value];		
	}
}

void not(NSTRUC *np)
{

	np->value = table_not[np->unodes[0]->value];
		
}

void not_2(NSTRUC *np)
{
	np->value = table_not[np->value];
}