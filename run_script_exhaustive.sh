# Read command line arguments
circuit=$1
atpg=$2
fault_sim=$3

# Display arguments
echo -e "Circuit is         : $circuit"
echo -e "ATPG is            : $atpg"
echo -e "Fault Simulator is : $fault_sim"

# Check if arguments exists
if [ -z $circuit ] 
then
    echo -e "Circuit undefined !!!"
    exit 1
fi

if [ -z $atpg ] 
then
    echo -e "ATPG undefined !!!"
    exit 1
fi

if [ -z $fault_sim ] 
then
    echo -e "Fault Simulator undefined !!!"
    exit 1
fi

# Display the ckt file path
ckt_path="./circuits/""$circuit"".ckt"
echo -e "\nCircuit path is $ckt_path\n"


# Create logs folder
log_path="./logs_exhaustive/"
if [ -d "$log_path" ] 
then
    echo -e "\nLog directory already present\n"
else
    mkdir $log_path
fi

# Create session folder
time_stamp=$(date +%Y_%m_%d_%H_%M_%S)
session_folder=$log_path$time_stamp"/"
session_log=$session_folder"session.log"
mkdir $session_folder

# Create symbolic Link
soft_link=$log_path"latest"
if [ -L $soft_link ]; then
    echo -e "\nDeleting old softlink\n"
    rm $soft_link
fi
cd $log_path
ln -s $time_stamp latest
cd ../

# Create file paths 
vector_file=$session_folder$circuit"."$atpg
fault_sim_output=$session_folder$circuit"."$fault_sim

echo -e "Circuit is         : $circuit" > $session_log
echo -e "ATPG is            : $atpg" >> $session_log
echo -e "Fault Simulator is : $fault_sim" >> $session_log

# Initialise fault list
if [ $circuit == "c17" ] 
then
    fault_list="golden_files/c17/test_case_1/c17_fault_list.txt"
    echo -e "Full Fault list is : $fault_list\n"
    echo -e "Full Fault list is : $fault_list\n" >> $session_log
elif [ $circuit == "c880" ] 
then
    fault_list="golden_files/c880/c880_fault_list.txt"
    echo -e "Full Fault list is : $fault_list\n"
    echo -e "Full Fault list is : $fault_list\n" >> $session_log
elif [ $circuit == "c1355" ] 
then
    fault_list="golden_files/c1355/c1355_fault_list.txt"
    echo -e "Full Fault list is : $fault_list\n"
    echo -e "Full Fault list is : $fault_list\n" >> $session_log
else
    echo -e "\nINVALID CIRCUIT NAME !!!\n"
    exit 1;
fi

# Run exhaustive ATPG
if [ $atpg == "podem" ] 
then
    echo -e "\nCompiling code for PODEM\n"
    make -C ./podem/ clean
    make -C ./podem/
    time ./podem/podem $ckt_path $fault_list $vector_file
elif [ $atpg == "d_algo" ] 
then
    echo -e "Compiling code for D_Algo\n"
    make -C ./pre_processor/ clean
    make -C ./pre_processor/
    cd ./pre_processor
    java DAlgoTop ../$ckt_path ../$fault_list ../$vector_file
    cd ../
else
    echo -e "\nINVALID ATPG ALGO NAME !!!\n"
    exit 1
fi

# Run exhaustive Fault Simulator
if [ $fault_sim == "dfs" ] 
then
    echo -e "\nCompiling code for DFS\n"
    make -C ./deductive_fault_simulation/src/ clean
    make -C ./deductive_fault_simulation/src/
    time ./deductive_fault_simulation/src/deductive_fault $ckt_path $vector_file $fault_list $fault_sim_output 
elif [ $fault_sim == "pfs" ] 
then
    echo -e "Compiling code for PFS\n"
    make -C ./parallel_fault_simulation/ clean
    make -C ./parallel_fault_simulation/
    time ./parallel_fault_simulation/pfs $ckt_path $vector_file $fault_list $fault_sim_output
else
    echo -e "\nINVALID FAULT SIMULATOR NAME !!!\n"
    exit 1
fi
