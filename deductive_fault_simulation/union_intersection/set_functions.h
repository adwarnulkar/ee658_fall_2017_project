#ifndef __SET_FUNCTIONS__
#define __SET_FUNCTIONS__

extern int binarySearch(int arr[], int l, int r, int x);

extern void SIZE_OF_UNION_INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* union_l , int* intersect_l);

extern int SIZE_OF_UNION (int* list_1 , int* list_2 , int length_1 , int length_2);

extern int SIZE_OF_INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2);

extern void quickSort(int arr[], int low, int high);

extern int partition (int arr[], int low, int high);

extern void swap(int* a, int* b);

extern void print_list(int* list , int length);

extern int* UNION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* union_length);

extern int* INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* intersect_length);

extern int* MINUS (int* list_1 , int* list_2 , int length_1 , int length_2 , int minus_length);

#endif
