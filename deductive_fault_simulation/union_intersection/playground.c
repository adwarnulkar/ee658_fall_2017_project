#include <stdio.h>
#include <stdlib.h>

#include "set_functions.h"

/*
//ORIGINAL TEST CODE
int main (void) {

    int length_1;
    int length_2;
    
    //Read length of list_1
    printf("Enter the size of LIST_1 : ");
    scanf("%d",&length_1);

    //Dynamically allocate memory for list_1
    int *list_1 = malloc(length_1 * sizeof(int));

    //Read elements of list_1
    printf("\nEnter the %d elements of LIST 1:\n",length_1);
    for (int i = 0 ; i < length_1 ; i++) {
        scanf("%d", &list_1[i]);
    }

    //Read length of list_2
    printf("\nEnter the size of LIST_2 : ");
    scanf("%d",&length_2);

    //Dynamically allocate memory for list_2
    int *list_2 = malloc(length_2 * sizeof(int));

    //Read elements of list_2
    printf("Enter the %d elements of LIST 2:\n",length_2);
    for (int i = 0 ; i < length_2 ; i++) {
        scanf("%d", &list_2[i]);
    }

    //Print elemts of LIST_1
    printf("\nElements of LIST_1 are :\n");
    print_list(list_1,length_1);

    //Print elemts of LIST_2
    printf("\nElements of LIST_2 are :\n");
    print_list(list_2,length_2);

    printf("\n===================================\n");
    int union_length;
    int intersect_length;
    SIZE_OF_UNION_INTERSECTION(list_1 , list_2 , length_1 , length_2 , &union_length , &intersect_length);
    printf("UNION LENGTH : %d\n",union_length);
    printf("INTERSECT LENGTH : %d\n",intersect_length);


    printf("\n===================================\n");
    printf("Verifying SIZE_OF_UNION\n");
    union_length = SIZE_OF_UNION(list_1 , list_2 , length_1 , length_2);
    printf("UNION LENGTH : %d\n",union_length);

    printf("\n===================================\n");
    printf("Verifying SIZE_OF_INTERSECTION\n");
    intersect_length = SIZE_OF_INTERSECTION(list_1 , list_2 , length_1 , length_2);
    printf("INTERSECTION LENGTH : %d\n",intersect_length);

    printf("\n===================================\n");
    printf("Lists after function call\n");
    //Print elemts of LIST_1
    printf("\nElements of LIST_1 are :\n");
    print_list(list_1,length_1);

    //Print elemts of LIST_2
    printf("Elements of LIST_2 are :\n");
    print_list(list_2,length_2);

    //Dynamically create a union list
    printf("\n===================================\n");
    printf("Evaluating UNION");
    int* list_union;
    list_union = UNION(list_1,list_2,length_1,length_2,union_length);
    printf("Union of the lists is :\n");
    print_list(list_union,union_length);

    //Dynamically create a intersect list
    printf("\n===================================\n");
    printf("Evaluating INTERSECTION");
    int* list_intersect;
    list_intersect = INTERSECTION(list_1,list_2,length_1,length_2,intersect_length);
    printf("Intersection of the lists is :\n");
    print_list(list_intersect,intersect_length);

    //Dynamically create a minus list
    printf("\n===================================\n");
    printf("Evaluating MINUS\n");
    int minus_length = length_1 - SIZE_OF_INTERSECTION(list_1,list_2,length_1,length_2);
    int* list_minus;
    list_minus = MINUS(list_1,list_2,length_1,length_2,minus_length);
    printf("Minus of the lists is :\n");
    print_list(list_minus,minus_length);

}
*/

/* 
//TEST_1 : INTERSECTION OF TWO LISTS
int main (void){
    int N = 2;

    int* length = malloc(N*sizeof(int));
    length[0] = 4;
    length[1] = 6;

    int** list;
    list = malloc(N*sizeof(int*));
    for (int i=0 ; i<N ; i++) {
        list[i] = malloc(length[i]*sizeof(int));
    }

    //Read list from user
    for(int i=0 ; i<N ; i++){
        printf("\nEnter the elements of list %d\n",i);
        for(int j=0 ; j<length[i] ; j++) {
            scanf("%d",&list[i][j]);
        }
    }

    //print list
    printf("\nLists are : \n");
    for (int i=0 ; i<N ; i++){
        print_list(list[i],length[i]);
    }

    int length_int = 0;
    int* list_intersect = INTERSECTION(list[0],list[1],length[0],length[1],&length_int);

    print_list(list_intersect,length_int);

    free(list_intersect);
    for(int i=0 ; i<N ; i++){
        free(list[i]);
    }
    free(list);

}
*/

int main (void){
    int N = 2;

    int* length = malloc(N*sizeof(int));
    length[0] = 4;
    length[1] = 6;

    int** list;
    list = malloc(N*sizeof(int*));
    for (int i=0 ; i<N ; i++) {
        list[i] = malloc(length[i]*sizeof(int));
    }

    //Read list from user
    for(int i=0 ; i<N ; i++){
        printf("\nEnter the elements of list %d\n",i);
        for(int j=0 ; j<length[i] ; j++) {
            scanf("%d",&list[i][j]);
        }
    }

    //print list
    printf("\nLists are : \n");
    for (int i=0 ; i<N ; i++){
        print_list(list[i],length[i]);
    }

    int length_union = 0;
    int* list_union = UNION(list[0],list[1],length[0],length[1],&length_union);

    printf("\nUnion Length : %d\n",length_union);
    print_list(list_union,length_union);

    free(list_union);
    for(int i=0 ; i<N ; i++){
        free(list[i]);
    }
    free(list);

}

/*
//TEST_1 : TESTING INTERSECTION ENSEMBLE
int main (void) {

    int N = 6; //Number of inputs to AND gate

    int* length = malloc(N*sizeof(int));
    int* inval  = malloc(N*sizeof(int));

    for(int i=0 ; i<N ; i++) {
        length[i] = 4;
    }

    int** list;
    list = malloc(N*sizeof(int*));
    for (int i=0 ; i<N ; i++) {
        list[i] = malloc(length[i]*sizeof(int));
    }

    //Read list from user
    for(int i=0 ; i<N ; i++){
        printf("\nEnter the elements of list %d\n",i);
        for(int j=0 ; j<length[i] ; j++) {
            scanf("%d",&list[i][j]);
        }
    }
    
    //print list
    printf("\nLists are : \n");
    for (int i=0 ; i<N ; i++){
        print_list(list[i],length[i]);
    }

    //Take node values from user
    printf("\nEnter the input vector : \n");
    for (int i=0 ; i<N ; i++) {
        scanf("%d",&inval[i]);
    }

    printf("\nUser input vector : \n");
    print_list(inval,N);

    //Check the type of list formula
    int nc_count = 0;
    for(int i=0 ; i<N ; i++){
        if(inval[i] == 0) {
            nc_count++;
        }
    }

    int* L_int = NULL; int length_int=0;
    int* L_union = NULL; int length_union=0;
    if(nc_count >= 1){
        printf("\nAtleast one non-controlling input present\n");

        //Initialize L_int and L_union
        int nc_found = 0;
        int c_found = 0;
        for(int i=0 ; i<N ; i++){
            if(inval[i] == 0 && (c_found == 0)) {
                L_int = list[i];
                length_int = length[i];
                c_found = 1;
            }
            if(inval[i] == 1 && (nc_found == 0)) {
                L_union = list[i];
                length_union = length[i];
                nc_found = 1;
            }

            if (c_found==1 && nc_found==1) {
                break;
            }
        }

    }

    print_list(L_int,length_int);
    print_list(L_union,length_int);

    //Compute ensemble union and intersection
    for (int i=0 ; i<N ; i++) {
        if (inval[i] == 0) {
            L_int = INTERSECTION(L_int,list[i],length_int,length[i],&length_int);
        }
    }

    print_list(L_int,length_int);

}
*/


int main (void) {

    int N = 3; //Number of inputs to AND gate

    int* length = malloc(N*sizeof(int));
    int* inval  = malloc(N*sizeof(int));

    for(int i=0 ; i<N ; i++) {
        length[i] = 2*(i+1);
    }

    int** list;
    list = malloc(N*sizeof(int*));
    for (int i=0 ; i<N ; i++) {
        list[i] = malloc(length[i]*sizeof(int));
    }

    //Read list from user
    for(int i=0 ; i<N ; i++){
        printf("\nEnter the elements of list %d\n",i);
        for(int j=0 ; j<length[i] ; j++) {
            scanf("%d",&list[i][j]);
        }
    }
    
    //print list
    printf("\nLists are : \n");
    for (int i=0 ; i<N ; i++){
        print_list(list[i],length[i]);
    }

    //Take node values from user
    printf("\nEnter the input vector : \n");
    for (int i=0 ; i<N ; i++) {
        scanf("%d",&inval[i]);
    }

    printf("\nUser input vector : \n");
    print_list(inval,N);

    int* L_union = NULL; int length_union=0;
    L_union = list[0];
 

    //print_list(L_int,length_int);
    print_list(L_union,length_union);

    //Compute ensemble union and intersection
    for (int i=0 ; i<N ; i++) {
        L_union = UNION(L_union,list[i],length_union,length[i],&length_union);
    }

    //print_list(L_int,length_int);
    printf("\nFINAL UNION :\n");
    print_list(L_union,length_union);

}

