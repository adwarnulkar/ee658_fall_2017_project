#ifndef __SET_FUNCTIONS__
#define __SET_FUNCTIONS__

extern int binarySearch(int arr[], int l, int r, int x);

extern void SIZE_OF_UNION_INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* union_l , int* intersect_l);

extern int SIZE_OF_UNION (int* list_1 , int* list_2 , int length_1 , int length_2);

extern int SIZE_OF_INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2);

extern void quickSort(int arr[], int low, int high);

extern int partition (int arr[], int low, int high);

extern void swap(int* a, int* b);

extern void print_list(int* list , int length);

extern int* UNION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* union_length);

extern int* INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* intersect_length);

extern int* MINUS (int* list_1 , int* list_2 , int length_1 , int length_2 , int* minus_length);

extern int cread();

extern void lev();

extern void simple_logic_simulator();

extern void fault_generation ();

enum e_ntype {GATE, PI, FB, PO};    /* column 1 of circuit format */
enum e_gtype {IPT, BRCH, XOR, OR, NOR, NOT, NAND, AND};  /* gate types */

#define MAXLINE 500               /* Input buffer size */

#define MAXNAME 500               /* File name size */

char input_vector_file[MAXNAME];

char output_vector_file[MAXNAME];

char fault_vector_file[MAXNAME];

char ckt_name [MAXNAME];

typedef struct n_struc {
   unsigned indx;             /* node index(from 0 to NumOfLine - 1 */
   unsigned num;              /* line number(May be different from indx */
   enum e_gtype type;         /* gate type */
   unsigned fin;              /* number of fanins */
   unsigned fout;             /* number of fanouts */
   struct n_struc **unodes;   /* pointer to array of up nodes */
   struct n_struc **dnodes;   /* pointer to array of down nodes */
   int level;                 /* level of the gate output */
   int value;		      	  /* value of node after simulation */
   int *list;				  /* inter pointer that contains the fault list for that particular node */
   int list_length;			  /* fault list length */
   int c;    				  /* to determine the controlling value of a gate ex. for AND gate c = 0*/
} NSTRUC;

/*------------------------------------------------------------------------*/
NSTRUC *Node;                   /* dynamic array of nodes */
NSTRUC **Pinput;                /* pointer to array of primary inputs */
NSTRUC **Poutput;               /* pointer to array of primary outputs */
int Nnodes;                     /* number of nodes */
int Npi;                        /* number of primary inputs */
int Npo;                        /* number of primary outputs */
/*------------------------------------------------------------------------*/


#endif
