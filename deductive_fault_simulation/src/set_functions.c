/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <stdlib.h>
#include "deductive_fault_simulator.h"
// A utility function to swap two elements
void swap(int* a, int* b)
{
    int t = *a;
    *a = *b;
    *b = t;
}
 
/* This function takes last element as pivot, places
   the pivot element at its correct position in sorted
    array, and places all smaller (smaller than pivot)
   to left of pivot and all greater elements to right
   of pivot */
int partition (int arr[], int low, int high)
{
    int pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element
	int j;
    for (j = low; j <= high- 1; j++)
    {
        // If current element is smaller than or
        // equal to pivot
        if (arr[j] <= pivot)
        {
            i++;    // increment index of smaller element
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}
 
/* The main function that implements QuickSort
 arr[] --> Array to be sorted,
  low  --> Starting index,
  high  --> Ending index */
void quickSort(int arr[], int low, int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
           at right place */
        int pi = partition(arr, low, high);
 
        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

/* A recursive binary search function. It returns 
   location of x in given array arr[l..r] is present, 
   otherwise -1
   Source: http://www.geeksforgeeks.org/find-union-and-intersection-of-two-unsorted-arrays/
*/
int binarySearch(int arr[], int l, int r, int x)
{
    if (r >= l)
    {
        int mid = l + (r - l)/2;
 
        // If the element is present at the middle itself
        if (arr[mid] == x)  return mid;
 
        // If element is smaller than mid, then it can only
        // be present in left subarray
        if (arr[mid] > x) 
          return binarySearch(arr, l, mid-1, x);
 
        // Else the element can only be present in right subarray
        return binarySearch(arr, mid+1, r, x);
    }
 
    // We reach here when element is not present in array
    return -1;
}

void SIZE_OF_UNION_INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2 , int *union_l , int *intersect_l) {

    *union_l = 0;
    *intersect_l = 0;


    //Find size of intersection 
    //TODO: Need to avoid below code (BRAINSTORM with team)
    if (length_1 > length_2)
    {
        int *tempp = list_1;
        list_1 = list_2;
        list_2 = tempp;
 
        int temp = length_1;
        length_1 = length_2;
        length_2 = temp;
    }
 
    // Now list_1[] is smaller
    // Sort smaller array list1[0..m-1]
    quickSort(list_1,0,length_1-1);
 
    // Search every element of bigger array in smaller
    // array and print the element if found
    int i;
	for (i=0; i < length_2; i++)
        if (binarySearch(list_1, 0, length_1-1, list_2[i]) != -1)
            *intersect_l = *intersect_l + 1;

    //debug: printf("Size of intersection is : %d\n",intersect_count);
    *union_l = length_1 + length_2 - *intersect_l;

}

int SIZE_OF_INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2) {

    int intersect_l = 0;

    //Find size of intersection 
    //TODO: Need to avoid below code (BRAINSTORM with team)
    if (length_1 > length_2)
    {
        int *tempp = list_1;
        list_1 = list_2;
        list_2 = tempp;
 
        int temp = length_1;
        length_1 = length_2;
        length_2 = temp;
    }
 
    // Now list_1[] is smaller
    // Sort smaller array list1[0..m-1]
    quickSort(list_1,0,length_1-1);
 
    // Search every element of bigger array in smaller
    // array and print the element if found
    int i;
	for (i=0; i < length_2; i++)
        if (binarySearch(list_1, 0, length_1-1, list_2[i]) != -1)
            intersect_l++;

    return intersect_l;
}

int SIZE_OF_UNION (int* list_1 , int* list_2 , int length_1 , int length_2) {

    int union_l;

    union_l = length_1 + length_2 - SIZE_OF_INTERSECTION(list_1,list_2,length_1,length_2);

    return union_l;
}


void print_list (int* list , int length) {
	int i;
    for (i = 0 ; i < length ; i++)
        printf("%d ",list[i]);

    printf("\n");
}

int* UNION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* union_length) {

    *union_length = SIZE_OF_UNION(list_1,list_2,length_1,length_2);
    
    int* list_union = (int* )malloc((*union_length) * sizeof(int));
	int i;
    //Find size of intersection 
    //TODO: Need to avoid below code (BRAINSTROM with team)
    if (length_1 > length_2)
    {
        int *tempp = list_1;
        list_1 = list_2;
        list_2 = tempp;
 
        int temp = length_1;
        length_1 = length_2;
        length_2 = temp;
    }

    quickSort(list_1,0,length_1-1);

    for (i=0; i < length_1 ; i++)
        list_union[i] = list_1[i];
 
    // Search every element of bigger array in smaller array
    // and print the element if not found
	int count = 0;
    for (i=0; i < length_2 ; i++)
        if (binarySearch(list_1, 0, length_1-1, list_2[i]) == -1){
            list_union[count+length_1] = list_2[i];
		count++;}
    return list_union;
 
}

int* INTERSECTION (int* list_1 , int* list_2 , int length_1 , int length_2 , int* intersect_length) {
    
    
    *intersect_length = SIZE_OF_INTERSECTION(list_1 , list_2 , length_1 , length_2);

    int* list_intersect = (int* )malloc((*intersect_length)*sizeof(int));

    // Before finding intersection, make sure list_1[0..m-1] 
    // is smaller
    if (length_1 > length_2)
    {
        int *tempp = list_1;
        list_1 = list_2;
        list_2 = tempp;
 
        int temp = length_1;
        length_1 = length_2;
        length_2 = temp;
    }
 
    // Now list_1[] is smaller
 
    // Sort smaller array list_1[0..m-1]
    quickSort(list_1,0,length_1-1);
 
    // Search every element of bigger array in smaller
    // array and print the element if found
    int count = 0;
	int i;
    for (i=0; i < length_2 ; i++)
        if (binarySearch(list_1, 0, length_1-1, list_2[i]) != -1) {
            list_intersect[count] = list_2[i];
            count++;
        }

    return list_intersect;
}

int* MINUS (int* list_1 , int* list_2 , int length_1 , int length_2 , int* minus_length) {

    
	*minus_length = length_1 - SIZE_OF_INTERSECTION(list_1, list_2, length_1, length_2);
	int* list_minus = (int* )malloc((*minus_length) * sizeof(int));
    //sort list_2
    quickSort(list_2,0,length_2-1);

    //parse through list 1 and print elements that are not in list_2
    int count = 0;
	int i;
    for(i=0 ; i < length_1 ; i++) {
        if(binarySearch(list_2, 0, length_2-1, list_1[i]) == -1){
            list_minus[count] = list_1[i];
            count++;
        }
    }
	
    return list_minus;
}


