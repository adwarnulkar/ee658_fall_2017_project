/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "deductive_fault_simulator.h"

void lev ()
{
	int i,j,all_ini,max;
	
	NSTRUC *np;
	for (i=0;i<Npi;i++)
		Pinput[i]->level = 0;
	all_ini=Nnodes-Npi;
	while (all_ini > 0)
	{
		for(i = 0; i<Nnodes; i++)
		{
			np = &Node[i];
			if (np->type != IPT){
				int count = 0; int arr[np->fin];
				memset(arr,-1,np->fin);
				for(j = 0; j<np->fin; j++)
				{	
					if (np->unodes[j]->level == -1) break;
					arr[j]=np->unodes[j]->level;
					count++;	
				}
				max = arr[0];
				if (count == np->fin)
				{
					for(j = 0; j<np->fin; j++){
						if (arr[j] > max)
							max = arr[j];
					}
					if (np->level == -1)
						all_ini--;
					np->level=max+1;
					//all_ini--;	
				}	
			}
		}
	}
}
