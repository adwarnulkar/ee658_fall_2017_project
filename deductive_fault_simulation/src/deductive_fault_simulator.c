/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>       

#include "deductive_fault_simulator.h"

/*This functions does the following tasks
1. performs logic simualtio
2. Asigns contrlling value of a gate
3. prints in the output vector file whether the given fault was detected by the given input test vector
4. prints the totsl faults that were detected by the given testvectors on the screen
5. prints the fault coverage on the screen*/
void simple_logic_simulator()
{
	int i,i1,l,k,h,b, max_level;
	l=1;b=1;
	NSTRUC *np;
	int *final_fault_list;
	int final_fault_list_length;
	int fault_code;
	final_fault_list = Poutput[0]->list;
	final_fault_list_length = Poutput[0]->list_length;
	FILE *fd1,*fd2,*fd3;
	char c[MAXLINE],c1[MAXLINE];
	max_level=0;
	for(i = 0; i<Nnodes; i++)
	{	np = &Node[i];
		if (np->level > max_level)
		max_level=np->level;
	}		
	if((fd1 = fopen(input_vector_file,"r")) == NULL)
      		printf("input vector file for logic simulation does not exist\n" );
	fd2 = fopen(output_vector_file,"w");
	
	if((fd3 = fopen(fault_vector_file,"r")) == NULL)
      		printf("Fault file for logic simulation does not exist\n" );
	
	int test_num = 0;
	while ((fgets(c,MAXLINE,fd1)) != NULL)
	{	
		fgets(c1,MAXLINE,fd3);
		fault_code = atoi(c1);

		if (c[0] != '-')
		{
			  
			  for (i1=0;i1<Npi;i1++)
			  {		
				Pinput[i1]->value = c[i1] - '0';
			  }
				
			for (l=1;l <= max_level;l++){		
				for(i = 0; i<Nnodes; i++)
				{
					np = &Node[i];
					if (np->level == l){	
								if(np->type == BRCH) {				//branch
									for (h=0;h<np->fin;h++)
									np->value = np->unodes[h]->value;
								}
								else if(np->type == XOR) {		//XOR
									h=0;
									np->value = np->unodes[h]->value; 
									for (h=1;h<np->fin;h++)
									np->value = (np->value) ^ (np->unodes[h]->value);
								}
								else if(np->type == OR) {		//OR
									h=0;
									np->value = np->unodes[h]->value; 
									for (h=1;h<np->fin;h++)
										np->value = (np->value) | (np->unodes[h]->value);
									np->c = 1;
								}
								else if(np->type == NOR) {		//NOR
									h=0;
									np->value = np->unodes[h]->value; 
									for (h=0;h<np->fin;h++)
										np->value = (((np->value) | (np->unodes[h]->value)));
									np->value = b & (~(np->value));
									np->c = 1;
								}
								else if(np->type == NOT) {		//NOT
									h=0;
									np->value = np->unodes[h]->value;
									np->value = b & (~(np->value));
								}
								else if(np->type == NAND) {		//NAND
									h=0;
									np->value = np->unodes[h]->value; 
									for (h=1;h<np->fin;h++)
										np->value = (np->value) & (np->unodes[h]->value);
									np->value = b & (~(np->value));
									np->c = 0;
								}
								else if(np->type == AND){			//AND
									h=0;						
									np->value = np->unodes[h]->value; 
									for (h=1;h<np->fin;h++)
										np->value = (np->value) & (np->unodes[h]->value);
									np->c = 0;
								}	
					}
				}			
			}
			
			fault_generation();
			int p;
			int fault_flag;
			test_num++;
			//fprintf(fd2,"Test Vector %d is : ",test_num);
			//fprintf(fd2,"%s",c);
			fault_flag = 0;
			for (k=0;k<Npo;k++){
				
				//fprintf(fd2,"Output IDX %d fault list is : ",Poutput[k]->indx);

				//Sorting the list for comparision with golden result (DISABLE THIS FOR DEMO)
				quickSort(Poutput[k]->list,0,Poutput[k]->list_length - 1);

				for (p=0; p < (Poutput[k]->list_length); p++){
					//fprintf(fd2,"%d ",Poutput[k]->list[p]);
					if (fault_code == Poutput[k]->list[p]) {
						fault_flag = 1;
						break;
					}
				}
				//fprintf(fd2, "\n");

				final_fault_list = UNION(final_fault_list, Poutput[k]->list, final_fault_list_length, Poutput[k]->list_length, &final_fault_list_length);
			}
			//fprintf(fd2,"\n");
			if (fault_flag == 1){
				fprintf(fd2, "FAULT_DETECTED ;; Fault is %4d ;; test_vector is %s",fault_code, c);
			}
			else {
				fprintf(fd2, "FAULT_NOT_DETECTED ;; Fault is %4d ;; test_vector is %s",fault_code, c);
			}
		}
		else
		{
            fprintf(fd2, "FAULT_NOT_DETECTED ;; Fault is %4d ;; test_vector is ---\n",fault_code);
		}
	}

	fclose(fd1);	
	fclose(fd2);
	fclose(fd3);

	printf ("\nCOMPLETE LIST OF FAULTS = \n");
	print_list(final_fault_list, final_fault_list_length);

	float percent = (final_fault_list_length / (2.0*Nnodes)) * 100.0;
	printf("\nFAULT COVERAGE = %f percent\n", percent);
}

/*This function does the following
1. Generates fault list for the particular node
2. Finds the fault list length
above 2 task is achieved by making use of functions present in set_functions.c file*/
void fault_generation (){
	int i=0,l,j;
	int max_level=0;int *temp;
	NSTRUC *np;
	for(i = 0; i<Nnodes; i++)
	{	np = &Node[i];
		if (np->level > max_level)
		max_level=np->level;
	}

	for (l=0;l <= max_level;l++){	//made l = 0 from l = 1	
	int g = 0;
			for(i = 0; i<Nnodes; i++) //TODO: can we reduce
			{
				np = &Node[i];
				if (np->level == l){
					
					if (np->level == 0){
						int size_input = 1;
						
						temp = (int* )malloc(Npi * sizeof(int));
						if(np->value == 1)
							temp[g] = ((np->indx * 10) + 0);
						else
							temp[g] = ((np->indx * 10) + 1);
						np->list = &temp[g];
						np->list_length = size_input;
						//printf("Index Number %d: ", np->indx);//debug
						//print_list(np->list, np->list_length);//debug								
						g++;
							
					}
					
					else if ((np->type == BRCH) || (np->type == NOT)){  // BRANCH , NOT
						int fault_of_cur_node[1];
						if(np->value == 1)
							fault_of_cur_node[0] = ((Node[i].indx * 10) + 0);
						else
							fault_of_cur_node[0] = ((Node[i].indx * 10) + 1);
						int h = 0;
						np->list = UNION(np->unodes[h]->list, fault_of_cur_node,np->unodes[h]->list_length, 1, &(np->list_length));
						//printf("Index Number %d: ", np->indx);//debug
						//print_list(np->list, np->list_length);		//debug
					}
					
					else if(np->type == XOR){		//XOR
						int *l_union, *l_inter, l_union_len, l_inter_len;
						l_union = np->unodes[0]->list;
						l_union_len = np->unodes[0]->list_length; 
						l_inter = np->unodes[0]->list;
						l_inter_len = np->unodes[0]->list_length; 
						
						int fault_of_cur_node[1], *size_of_cur_node, cur_node_size = 1;
						size_of_cur_node = &cur_node_size;
						if(np->value == 1)
							fault_of_cur_node[0] = ((np->indx * 10) + 0);
						else
							fault_of_cur_node[0] = ((np->indx * 10) + 1);
						
						for (j=0; j<np->fin; j++){
							l_union = UNION(l_union, np->unodes[j]->list, l_union_len, np->unodes[j]->list_length, &l_union_len);
						}
						
						for (j=0; j<np->fin; j++){
							l_inter = INTERSECTION(l_inter, np->unodes[j]->list, l_inter_len, np->unodes[j]->list_length, &l_inter_len);
						}
						
						//np->list = MINUS(l_union, l_inter, l_union_len, l_inter_len, &np->list_length);
						np->list = MINUS(l_union, l_inter, l_union_len, l_inter_len, &np->list_length);//added
						np->list = UNION(np->list, fault_of_cur_node,np->list_length, *size_of_cur_node, &np->list_length);;//added
						//printf("Index Number %d: ", np->indx);//debug
						//print_list(np->list, np->list_length); //debug
					}
					
					else { 		
								int all_cont = 0;
								int *f_union, *f_inter, len_union, len_inter;
								int fault_of_cur_node[1], *size_of_cur_node, cur_node_size = 1;
								size_of_cur_node = &cur_node_size;
								if(np->value == 1)
									fault_of_cur_node[0] = ((np->indx * 10) + 0);
								else
									fault_of_cur_node[0] = ((np->indx * 10) + 1);
								
								for (j=0; j<np->fin; j++){
									if (np->unodes[j]->value != (np->c))	{
										f_union = np->unodes[j]->list;
										len_union = np->unodes[j]->list_length;	
									}
									else {
										f_inter = np->unodes[j]->list;
										len_inter = np->unodes[j]->list_length;
										all_cont++;
									}
								}
								
								if (all_cont == 0) {	//all non controlling inputs
								
									for (j=0; j<np->fin; j++)
										f_union = UNION(f_union, np->unodes[j]->list, len_union, np->unodes[j]->list_length, &len_union);	
								
									np->list_length = j;
									np->list = UNION(f_union, fault_of_cur_node,len_union, *size_of_cur_node, &np->list_length);
																
								}							
								else{					//atleast 1 controlling
									if (all_cont == np->fin)
									{	f_union = NULL; len_union = 0;}
									
									for (j=0; j<np->fin; j++)
									{
										//if (np->unodes[j]->value == 1)
										if (np->unodes[j]->value != np->c)
										{
											f_union = UNION(f_union, np->unodes[j]->list, len_union, np->unodes[j]->list_length, &len_union);
										}
										else
										{
											f_inter = INTERSECTION(f_inter, np->unodes[j]->list, len_inter, np->unodes[j]->list_length, &len_inter);
										}		
									}
									
									f_inter = MINUS(f_inter, f_union, len_inter, len_union, &len_inter);
									np->list_length = j;
									np->list = UNION(f_inter, fault_of_cur_node,len_inter, *size_of_cur_node, &np->list_length);								
								}
								//printf("Index Number %d: ", np->indx);//debug
								//print_list(np->list, np->list_length);	//debug
					}	
					
				}
			}
	}
} 
