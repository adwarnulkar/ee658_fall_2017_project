import sys

def friedman_generator(x):
    M = x[0] and x[1];
    N = x[2] and x[3];
    G = M or N;
    P = not x[2];
    R = not x[3];
    Q = not x[4];
    K = P or Q or R;
    J = x[4] and x[5] and x[6];
    y = G and K and J;
    return y;


# Read the file name from the command line
input_file = sys.argv[1];
output_file_name = sys.argv[2];
out_file = open(output_file_name,'w');

# Read the inputs from the vector file
for line in open(input_file).readlines():
    line = line.rstrip('\n');
    x = list(line);
    x = map(int,x);

    # Choose the circuit to be simulated
    y = friedman_generator(x);

    out_file.write("%i\n" %(y));

# Close the file handle
out_file.close();

