# Author : Aditya Warnulkar
# Description : This file generate golden output for benchmark_1.ckt

import sys

def benchmark_1_generator(x):
    F = not (x[0] and x[1]);
    G = x[2];
    H = x[2];
    I = not (F and G);
    J = H or x[3];
    K = J and x[4];
    y = not (I or K);
    return y;


# Read the file name from the command line
input_file = sys.argv[1];
output_file_name = sys.argv[2];
out_file = open(output_file_name,'w');

# Read the inputs from the vector file
for line in open(input_file).readlines():
    line = line.rstrip('\n');
    x = list(line);
    x = map(int,x);

    # Choose the circuit to be simulated
    y = benchmark_1_generator(x);

    out_file.write("%i\n" %(y));

# Close the file handle
out_file.close();

