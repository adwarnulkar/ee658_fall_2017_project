import sys


def xor(a,b):
    c = (a and (not b)) or ((not(a)) and b);
    return c;

def goel_generator(x):
    H = x[0] and x[1];
    J = xor(x[2],x[3]);
    K = xor(x[4],x[5]);
    L = not xor(x[2],x[3]);
    M = not xor(x[4],x[5]);
    N = xor(J,K);
    P = xor(N,H);
    Q = not xor(L,M);
    y = xor(P,Q);
    return y;


# Read the file name from the command line
input_file = sys.argv[1];
output_file_name = sys.argv[2];
out_file = open(output_file_name,'w');

# Read the inputs from the vector file
for line in open(input_file).readlines():
    line = line.rstrip('\n');
    x = list(line);
    x = map(int,x);

    # Choose the circuit to be simulated
    y = goel_generator(x);

    out_file.write("%i\n" %(y));

# Close the file handle
out_file.close();

