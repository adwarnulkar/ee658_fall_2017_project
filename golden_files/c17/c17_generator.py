import sys

def c17_golden_generator(x):
    G1 = not (x[0] and x[2]);
    G2 = not (x[2] and x[3]);
    G3 = not (x[1] and G2);
    G4 = not (x[4] and G2);
    G5 = not (G1 and G3);
    G6 = not (G4 and G3);
    y = [G5,G6];
    return y;

def c17_nor_generator(x):
    G1 = not (x[0] or x[2]);
    G2 = not (x[2] or x[3]);
    G3 = not (x[1] or G2);
    G4 = not (x[4] or G2);
    G5 = not (G1 or G3);
    G6 = not (G4 or G3);
    y = [G5,G6];
    return y;

def c17_and_generator(x):
    G1 = (x[0] and x[2]);
    G2 = (x[2] and x[3]);
    G3 = (x[1] and G2);
    G4 = (x[4] and G2);
    G5 = (G1 and G3);
    G6 = (G4 and G3);
    y = [G5,G6];
    return y;

def c17_mix_1_generator(x):
    G1 = (x[0] or x[2]);
    G2 = (x[2] and x[3]);
    G3 = (x[1] and not(G2)) or (not(x[1]) and G2);
    G4 = (x[4] and not(G2)) or (not(x[4]) and G2);
    G5 = not (G1 and G3);
    G6 = not (G4 or G3);
    y = [G5,G6];
    return y;


# Read the file name from the command line
input_file = sys.argv[1];
output_file_name = sys.argv[2];
out_file = open(output_file_name,'w');

# Read the inputs from the vector file
for line in open(input_file).readlines():
    line = line.rstrip('\n');
    x = list(line);
    x = map(int,x);

    # Choose the circuit to be simulated
    #y = c17_golden_generator(x);
    #y = c17_nor_generator(x);
    #y = c17_and_generator(x);
    y = c17_mix_1_generator(x);

    out_file.write("%i%i\n" %(y[0],y[1]));

# Close the file handle
out_file.close();

