/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#ifndef __SIMPLE_LOGIC_SIMULATION__
#define __SIMPLE_LOGIC_SIMULATION__

extern void lev();			/* function that does levelisation*/
extern void simple_logic_simulator(); /*this function performs logic simulation*/

enum e_ntype {GATE, PI, FB, PO};    /* column 1 of circuit format */
enum e_gtype {IPT, BRCH, XOR, OR, NOR, NOT, NAND, AND};  /* gate types */

#define MAXLINE 500               /* Input buffer size */
#define MAXNAME 500               /* File name size */

char input_vector_file[MAXNAME];
char output_vector_file[MAXNAME];
char ckt_name [MAXNAME];

typedef struct n_struc {
   unsigned indx;             /* node index(from 0 to NumOfLine - 1 */
   unsigned num;              /* line number(May be different from indx */
   enum e_gtype type;         /* gate type */
   unsigned fin;              /* number of fanins */
   unsigned fout;             /* number of fanouts */
   struct n_struc **unodes;   /* pointer to array of up nodes */
   struct n_struc **dnodes;   /* pointer to array of down nodes */
   int level;                 /* level of the gate output */
   long long int value;		      //used to contain value in sls.c
} NSTRUC;

/*------------------------------------------------------------------------*/

NSTRUC *Node;                   /* dynamic array of nodes */
NSTRUC **Pinput;                /* pointer to array of primary inputs */
NSTRUC **Poutput;               /* pointer to array of primary outputs */
int Nnodes;                     /* number of nodes */
int Npi;                        /* number of primary inputs */
int Npo;                        /* number of primary outputs */

/*------------------------------------------------------------------------*/

#endif
