/*=======================================================================
  A simple parser for "self" format

  The circuit format (called "self" format) is based on outputs of
  a ISCAS 85 format translator written by Dr. Sandeep Gupta.
  The format uses only integers to represent circuit information.
  The format is as follows:

1        2        3        4           5           6 ...
------   -------  -------  ---------   --------    --------
0 GATE   outline  0 IPT    #_of_fout   #_of_fin    inlines
                  1 BRCH
                  2 XOR(currently not implemented)
                  3 OR
                  4 NOR
                  5 NOT
                  6 NAND
                  7 AND

1 PI     outline  0        #_of_fout   0
2 FB     outline  1 BRCH   inline
3 PO     outline  2 - 7    0           #_of_fin    inlines

=======================================================================

Author : Deven Rakeshkumar Gupta
Date   : 3rd November 2017

University of Southern California
Course : EE658 Testing of Digital Systems

Team : Group_12

=======================================================================*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "simple_logic_simulation.h"

int cread(char*);
void pc();
void clear();
void allocate();

int main(int argc, char *argv[])
{
    // Read command line arguments
    strcpy(ckt_name,argv[1]);
	strcpy(input_vector_file,argv[2]);
	strcpy(output_vector_file,argv[3]);

    // Parse the circuit file
    printf("\n=======================================================\n");
    printf("\nReading circuit file %s \n",ckt_name);
    cread(ckt_name);
    printf("\n=======================================================\n");

    // Perform Levelisation
    printf("\nPerforming Levelisation\n");
    lev();
    printf("\n=======================================================\n");

    // Print the circuit description
    pc();
    printf("\n=======================================================\n");

    // Perform Logic Simulation
    printf("\nPerforming Simple Logic Simulation\n");
    simple_logic_simulator();
    printf("\nSimple Logic Simulation Complete .... !!\n");
    printf("\nOutput file %s is generated .. !!\n",output_vector_file);
    printf("\n=======================================================\n");
    
    return 0;
}

/*This circuit reads the .ckt file and sets up necessary information such as number of fin,
number of fout, index value, node number, inputs to a gate etc. which is required for further stages*/
int cread(char *cp)
{
   char buf[MAXLINE];
   int ntbl, *tbl, i, j, k, nd, tp, ni = 0, no = 0;
   FILE *fd;
   NSTRUC *np;

   sscanf(cp, "%s", buf);
   if((fd = fopen(buf,"r")) == NULL) {
      printf("File %s does not exist!\n", buf);
      return 0;
   }
   Nnodes = Npi = Npo = ntbl = 0;
   while(fgets(buf, MAXLINE, fd) != NULL) {
      if(sscanf(buf,"%d %d", &tp, &nd) == 2) {
         if(ntbl < nd) ntbl = nd; 
         Nnodes ++;
         if(tp == PI) Npi++; 
         else if(tp == PO) Npo++;
      }
   }
   tbl = (int *) malloc(++ntbl * sizeof(int));

   fseek(fd, 0L, 0);
   i = 0;
   while(fgets(buf, MAXLINE, fd) != NULL) {
      if(sscanf(buf,"%d %d", &tp, &nd) == 2) tbl[nd] = i++;
   }
   allocate();

   fseek(fd, 0L, 0);
   while(fscanf(fd, "%d %d", &tp, &nd) != EOF) {
      np = &Node[tbl[nd]];
      np->num = nd;
      if(tp == PI) Pinput[ni++] = np;
      else if(tp == PO) Poutput[no++] = np;
	  //printf("node number = %d node type = %d\n", nd,tp);
      switch(tp) {
         case PI:
         case PO:
         case GATE:
            fscanf(fd, "%d %d %d", &np->type, &np->fout, &np->fin);
            break;

         case FB:
            np->fout = np->fin = 1;
            fscanf(fd, "%d", &np->type);
            break;

         default:
            printf("Unknown node type!\n");
            exit(-1);
         }
      np->unodes = (NSTRUC **) malloc(np->fin * sizeof(NSTRUC *));
      np->dnodes = (NSTRUC **) malloc(np->fout * sizeof(NSTRUC *));
      for(i = 0; i < np->fin; i++) {
         fscanf(fd, "%d", &nd);
         np->unodes[i] = &Node[tbl[nd]];
         }
      for(i = 0; i < np->fout; np->dnodes[i++] = NULL);
      }
   for(i = 0; i < Nnodes; i++) {
      for(j = 0; j < Node[i].fin; j++) {
         np = Node[i].unodes[j];
         k = 0;
         while(np->dnodes[k] != NULL) k++;
         np->dnodes[k] = &Node[i];
         }
      }
   fclose(fd);
   return 0;
   
}

/*This function is used to print necessary information of the screen*/
void pc()
{
   int i, j;
   NSTRUC *np;
   char *gname();

   printf("\nPrinting Circuit Description\n");
   printf("\n Node   Type \tIn     \t\t\tOut    \t\tLEVEL\n");
   printf("------ ------\t-------\t\t\t-------\t\t-----\n");

   for(i = 0; i<Nnodes; i++) {
      np = &Node[i];
      printf("\r%4d  %6s\t", np->num, gname(np->type));
      for(j = 0; j<np->fin; j++) printf("%d ",np->unodes[j]->num);
      printf("\t\t\t");
      for(j = 0; j<np->fout; j++) printf("%d ",np->dnodes[j]->num);
      printf("\t\t");
      printf("%d", np->level);
	  printf("\n");
   }

   printf("\nPrimary inputs:  ");
   for(i = 0; i<Npi; i++) printf("%d ",Pinput[i]->num);
   printf("\n");
   printf("Primary outputs: ");
   for(i = 0; i<Npo; i++) printf("%d ",Poutput[i]->num);
   printf("\n\n");
   printf("Number of nodes = %d\n", Nnodes);
   printf("Number of primary inputs = %d\n", Npi);
   printf("Number of primary outputs = %d\n", Npo);
  
}

/*This function clear the aloocated memory by malloc after the task is done*/
void clear()
{
   int i;

   for(i = 0; i<Nnodes; i++) {
      free(Node[i].unodes);
      free(Node[i].dnodes);
   }
   free(Node);
   free(Pinput);
   free(Poutput);
}

/*This function allocates memory using malloc()*/
void allocate()
{
   int i;

   Node = (NSTRUC *) malloc(Nnodes * sizeof(NSTRUC));
   Pinput = (NSTRUC **) malloc(Npi * sizeof(NSTRUC *));
   Poutput = (NSTRUC **) malloc(Npo * sizeof(NSTRUC *));
   for(i = 0; i<Nnodes; i++) {
      Node[i].indx = i;
      Node[i].fin = Node[i].fout = 0;
	Node[i].level = -1;
	Node[i].value = 0;	
   }
}

/*This function returns a character pointer indicating the type of gate by giving the approprite index assocated with that gate*/
char *gname(tp)
int tp;
{
   switch(tp) {
      case 0: return("PI");
      case 1: return("BRANCH");
      case 2: return("XOR");
      case 3: return("OR");
      case 4: return("NOR");
      case 5: return("NOT");
      case 6: return("NAND");
      case 7: return("AND");
   }
}

/*========================= End of program ============================*/

