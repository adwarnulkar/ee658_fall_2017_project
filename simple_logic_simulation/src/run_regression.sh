!/usr/bin/env bash
make clean
make
make clean_logs

log_file="../logs/log_file.txt";

if [ -f "$log_file" ]
then
    rm -rf ../logs/log_file.txt
fi

echo "Running c17.ckt" 
./simple_logic ../../golden_files/c17/test_case_1/c17.ckt ../../golden_files/c17/test_case_1/c17_input.txt ../logs/c17_output.txt > $log_file
echo "diff is" >> $log_file 
diff ../logs/c17_output.txt ../../golden_files/c17/test_case_1/c17_original_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file 

echo "Running friedman.ckt" 
./simple_logic ../../golden_files/friedman/friedman.ckt ../../golden_files/friedman/friedman_input.txt ../logs/friedman_output.txt >> $log_file
echo "diff is" >> $log_file 
diff ../logs/friedman_output.txt ../../golden_files/friedman/friedman_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file

echo "Running goel.ckt" 
./simple_logic ../../golden_files/goel/goel.ckt ../../golden_files/goel/goel_input.txt ../logs/goel_output.txt >> $log_file
echo "diff is" >> $log_file 
diff ../logs/goel_output.txt ../../golden_files/goel/goel_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file

echo "Running test_case_2 c17_and.ckt" 
./simple_logic ../../golden_files/c17/test_case_2/c17_and.ckt ../../golden_files/c17/test_case_2/c17_input.txt ../logs/c17_and_output.txt >> $log_file
echo "diff is" >> $log_file 
diff ../logs/c17_and_output.txt ../../golden_files/c17/test_case_2/c17_and_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file 

echo "Running test_case_3 c17_nor.ckt" 
./simple_logic ../../golden_files/c17/test_case_3/c17_nor.ckt ../../golden_files/c17/test_case_3/c17_input.txt ../logs/c17_nor_output.txt >> $log_file
echo "diff is" >> $log_file 
diff ../logs/c17_nor_output.txt ../../golden_files/c17/test_case_3/c17_nor_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file 

echo "Running test_case_4 c17_mix_1.ckt" 
./simple_logic ../../golden_files/c17/test_case_4/c17_mix_1.ckt ../../golden_files/c17/test_case_4/c17_input.txt ../logs/c17_mix_1_output.txt >> $log_file
echo "diff is" >> $log_file 
diff ../logs/c17_mix_1_output.txt ../../golden_files/c17/test_case_4/c17_mix_1_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file 

echo "Running benchmark_1" 
./simple_logic ../../golden_files/benchmark_1/benchmark_1.ckt ../../golden_files/benchmark_1/benchmark_1_input.txt ../logs/benchmark_1_output.txt >> $log_file
echo "diff is" >> $log_file 
diff ../logs/benchmark_1_output.txt ../../golden_files/benchmark_1/benchmark_1_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file 

echo "Running benchmark_2" 
./simple_logic ../../golden_files/benchmark_2/benchmark_2.ckt ../../golden_files/benchmark_2/benchmark_2_input.txt ../logs/benchmark_2_output.txt >> $log_file
echo "diff is" >> $log_file 
diff ../logs/benchmark_2_output.txt ../../golden_files/benchmark_2/benchmark_2_output.txt >> $log_file
echo "=============================================================" >> $log_file 
echo -e "\n\n" >> $log_file 

echo "Running c880.ckt" 
./simple_logic ../../golden_files/c880/c880.ckt ../../golden_files/c880/c880_input.txt ../logs/c880_output.txt >> $log_file
#TODO: echo "diff is" >> $log_file 
#TODO: diff ../logs/benchmark_2_output.txt ../../golden_files/benchmark_2/benchmark_2_output.txt >> $log_file
#TODO: echo "=============================================================" >> $log_file 
#TODO: echo -e "\n\n" >> $log_file 

echo "Running c880.ckt" 
./simple_logic ../../golden_files/c880/c880.ckt ../../golden_files/c880/c880_input.txt ../logs/c880_output.txt >> $log_file
#TODO: echo "diff is" >> $log_file 
#TODO: diff ../logs/benchmark_2_output.txt ../../golden_files/benchmark_2/benchmark_2_output.txt >> $log_file
#TODO: echo "=============================================================" >> $log_file 
#TODO: echo -e "\n\n" >> $log_file 

echo "Running c1355.ckt" 
./simple_logic ../../golden_files/c1355/c1355.ckt ../../golden_files/c1355/c1355_input.txt ../logs/c1355_output.txt >> $log_file
#TODO: echo "diff is" >> $log_file 
#TODO: diff ../logs/benchmark_2_output.txt ../../golden_files/benchmark_2/benchmark_2_output.txt >> $log_file
#TODO: echo "=============================================================" >> $log_file 
#TODO: echo -e "\n\n" >> $log_file 

