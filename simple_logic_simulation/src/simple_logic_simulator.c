/* Author(s) 
    Deven Rakeshkumar Gupta (devenrag@usc.edu)
    Aditya Warnulkar (warnulka@usc.edu)

   Team : Group-12
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "simple_logic_simulation.h"

/*This functions performs simple logic simulation after performing levelisation. 
Input vectors are read from a input vector file and simulation is performed to get 
corresponding output vector file*/
void simple_logic_simulator()
{
	int i,i1,l,k,h;
	int max_level;
	l=1;
	long long  int b=1;	
	NSTRUC *np;
	FILE *fd1,*fd2;
	char c[MAXLINE];
	max_level=0;
	for(i = 0; i<Nnodes; i++)
	{	np = &Node[i];
		if (np->level > max_level)
		max_level=np->level;
	}	

	/*open input vector file to read input vectors one by one*/
	if((fd1 = fopen(input_vector_file,"r")) == NULL)
      		printf("input vector file for logic simulation does not exist" );
	
	/*open out vector file to write output vectors*/
	fd2 = fopen(output_vector_file,"w");
	while ((fgets(c,MAXLINE,fd1)) != NULL)
	{
		for (i1=0;i1<Npi;i1++)
			Pinput[i1]->value = c[i1] - '0';
			
		for (l=1;l <= max_level;l++){		
			for(i = 0; i<Nnodes; i++)
			{
				np = &Node[i];
				if (np->level == l){	
							if(np->type == BRCH) {
								for (h=0;h<np->fin;h++)
								np->value = np->unodes[h]->value;
							}
							else if(np->type == XOR) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
								np->value = (np->value) ^ (np->unodes[h]->value);
							}
							else if(np->type == OR) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
									np->value = (np->value) | (np->unodes[h]->value);
							}
							else if(np->type == NOR) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=0;h<np->fin;h++)
									np->value = (((np->value) | (np->unodes[h]->value)));
								np->value = b & (~(np->value));
							}
							else if(np->type == NOT) {
								h=0;
								np->value = np->unodes[h]->value;
								np->value = b & (~(np->value));
							}
							else if(np->type == NAND) {
								h=0;
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
									np->value = (np->value) & (np->unodes[h]->value);
								np->value = b & (~(np->value));
							}
							else if(np->type == AND){
								h=0;						
								np->value = np->unodes[h]->value; 
								for (h=1;h<np->fin;h++)
									np->value = (np->value) & (np->unodes[h]->value);
							}		
				}	  
			}			
	    }
		for (k=0;k<Npo;k++){
			fprintf(fd2,"%lld",Poutput[k]->value);}
		
		fprintf(fd2,"\n");
	}
	fclose(fd1);	
	fclose(fd2);   
}
