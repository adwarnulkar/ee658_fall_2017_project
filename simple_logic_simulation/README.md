# Simple Logic Simulation (Steps to compile and run the code)

#### Access the source code
cd src


#### Compile the code and create the executable file
make clean 

make


#### Run Simple Logic Simulation
./simple_logic c17.ckt c17_input.txt c17_output.txt


#### General command
./simple_logic <ckt file path> <text file containing input vectors> <output file name>


#### Command to clean the .o and executable files
make clean


#### Clean log files **(NOT TO BE USED BY TA/GRADER)**
make clean_logs


#### File description
- Makefile : Create the executable
- parser.c : Reads the ckt file and creates the necessary datastructures
- levelisation.c : Implements levelisation
- simple_logic_simulator.c : Implements simple logic simulation
- simple_logic_simulation.h : encapsulates global variables , defines and function prototypes
- c17.ckt : Sample .ckt file
- c17_input.txt : Sample input vector file (exchaustive vectors for c17.ckt)
- c17_output.txt : Sample output of c17.ckt
- run_regression.sh : For internal use of the team **(NOT TO BE USED BY TA/GRADER)**


#### Contact details **(Group-12)**
- Aditya Warnulkar (warnulka@usc.edu)
- Deven Rakeshkumar Gupta (devenrag@usc.edu)
- Divya Nandihalli (nandihal@usc.edu)
- Sayantan Ghosh (sayantag@usc.edu)
