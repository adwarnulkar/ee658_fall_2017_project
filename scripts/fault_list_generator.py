# This program generates the fault list of all possible faults in the circuit
# Input : .ckt file
# Output : fault_list file containing all possible faults

import sys

# Read the command line arguments
ckt_file = sys.argv[1];
output_file_name = sys.argv[2];
out_file = open(output_file_name,'w');

# Count the number of lines in the file
# Note: Number of lines will correspond to the number of nodes in the circuit
num_lines = 0;
for line in open(ckt_file).readlines():
    num_lines = num_lines + 1;


for k in range(0,num_lines):
    index_num = k;
    fault_sa0 = 10*k;
    fault_sa1 = 10*k + 1;
    out_file.write("%i\n" %(fault_sa0));
    out_file.write("%i\n" %(fault_sa1));

out_file.close();

