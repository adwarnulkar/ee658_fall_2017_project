import sys

get_bin = lambda x, n: format(x, 'b').zfill(n)

# Read the command line arguments
num_inputs = int(sys.argv[1]);
output_file_name = sys.argv[2];
out_file = open(output_file_name,'w');

for k in range(0,2**num_inputs):
    #print get_bin( k , num_inputs);
    out_file.write(get_bin( k , num_inputs));
    out_file.write("\n");


