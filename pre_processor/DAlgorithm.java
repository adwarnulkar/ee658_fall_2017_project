import java.util.ArrayList;
import java.io.FileWriter;
import java.io.IOException;

/**
 * This class implements the D-Algorithm 
 * It uses the Circuit class to store the circuit, TestPatGen to do imply and check 
 * contains the implementation of D-Algorithm 
 * @author Divya Nandihalli
 *
 */
public class DAlgorithm {
	Circuit ckt;
	TestPatGenNew tg;
	int call_count;
	
	/**
	 * Constructor uses the circuit object
	 * @param ckt
	 * @throws IOException
	 */
	public DAlgorithm(Circuit ckt) throws IOException {
		this.ckt = ckt;
	}
	
	/**
	 * D_top is the wrapper for the recursive D-Algorithm
	 * @param faultLine
	 * @param faultType
	 * @param testVecFile
	 * @return
	 * @throws IOException
	 */
	public boolean D_top(int faultLine, int faultType, FileWriter testVecFile) throws IOException {
		tg = new TestPatGenNew(ckt, faultLine, faultType);
		
		//start with empty State(i.e, no assignments present in the first call)
		tg.setState(new State()); 
		
		//add the given fault to the assignment stack
		ArrayList<Assignment> assignStack = new ArrayList<Assignment>();
		assignStack.add(new Assignment(faultLine,faultType+2));
		
		//call_count is used to terminate the program if it runs for too long
		call_count = 0;
		
		//call the recursive D_rec function 
		boolean success =  D_rec(assignStack,new State(),0);
		
		//write the testvector using the current assignments
		if(success) testVecFile.write(tg.getTestVec());
		else testVecFile.write(tg.getTestVecDummy());
		
		return success;
	}
	
	/**
	 * D_rec implements the recursive D_algorithm
	 * Processes D_frontier till error is not at output
	 * process J_frontier once the error reaches the output
	 * @param assignStack (stack of assignments to be performed in the given call)
	 * @param currState (State object containing assignments and other data)
	 * @param depth - represents how deep in the recursion a given call is in (used for debugging purposes)
	 * @return boolean success (true) or failure (false)
	 * @throws IOException
	 */
	private boolean D_rec(ArrayList<Assignment> assignStack, State currState, int depth) throws IOException {
		
        call_count ++;
        //terminate the algorithm and fail if it runs for too long
        if(call_count > 100000){
          return false;
        }
		depth = depth + 1;
	
        //make a local copy of state - to allow backtracking 
		State localState = new State(currState);
		
		//set the state of tg to this state
		tg.setState(localState);
		
		//call imply and check for each of the assignment in the assignStack
		boolean implyAndCheckSuccess = tryAssignments(assignStack);
		if(!implyAndCheckSuccess) {
			return false;
		}
		
		//if error is not at PO, process the D-Frontier
		if(!localState.errorAtPO()) {
			
			//process D frontier
			int line = localState.getUntriedGatefromD();
			
			while(line != -1) {
				ArrayList<Assignment> newAssignStack =  propagate(line,localState);
                
				//call recursive D-Algo
				if(D_rec(newAssignStack,localState,depth)) return true;
				//back track if it fails
				else {					
					line = localState.getUntriedGatefromD();				
				}
			}
			
			//if error not at PO and there are no more untried lines in D-Frontier, return false
			return false;
		}
		
		//if error is at PO, process J-Frontier
		else {
			//process J frontier
			int line = localState.getJLine();
			
			while(line!=-1) {
				if(!JFrontTryLine(line,localState,depth)) {
					return false;
				}
				else {
					return true;
				}
			}
			
			//when there are no more lines in J-Frontier, return true
			return true;
			
		}
	}
	
	/**
	 * private function to try each line of the J-Frontier
	 * the function tries an assignment for an input of the J-Frontier gate and backtracks when it fails
	 * input assignment is made to a controlling value
	 * calls the D_rec function to make an assignment
	 * this function is called from D_rec function
	 * @param line
	 * @param localState
	 * @param depth
	 * @return
	 * @throws IOException
	 */
	private boolean JFrontTryLine(int line, State localState,int depth) throws IOException {
		
		Node n = ckt.getCircuitNodeList().get(line);
		
		//get the fan in of the given line
		ArrayList<Node> fanInNodes = n.getFanInNodes();
		int controllingVal = 4;
		ArrayList<Assignment> newAssignStack;
		
		//initialize the controlling value based on the type of the gate
		if(n.getGateType().equals("NAND")||n.getGateType().equals("AND")) controllingVal = 0;
		if(n.getGateType().equals("NOR") ||n.getGateType().equals("OR")) controllingVal = 1;
		
		//try assigning one input at a time, till it succeeds
		for(int i=0; i<fanInNodes.size();i++) {
			int index = fanInNodes.get(i).getIndex();
 			if(localState.getValAtIndex(index)==4) {
 				
 				//assign the input to controlling value
 				newAssignStack = new ArrayList<Assignment>();
				newAssignStack.add(new Assignment(index,controllingVal));
				
				//recursive call to the D_rec function with new assignment stack
				if(D_rec(newAssignStack,localState,depth)) return true;
			} 			
		}
		
		//if all input assignments fail, return false
		return false;
	}
	
	/**
	 * tryAssignments - private function which tries assignments by calling D_rec function
	 * @param assignStack
	 * @return
	 */
	private boolean tryAssignments(ArrayList<Assignment> assignStack) {
		for(int i=0; i<assignStack.size();i++) {
			Assignment assgnmt = assignStack.get(i);
			if(!tg.tryAssignment(assgnmt.getLine(), assgnmt.getVal())) return false;
		}
		
		return true;
	}
	
	/**
	 * Use this function to get the assignments that will be needed to propagate at a particular line
	 * @param line - line at which the assignment should be made for error propagation 
	 * @param state - current state
	 * @return
	 */
	private ArrayList<Assignment> propagate(int line, State state){
		Node n = ckt.getCircuitNodeList().get(line);
		if(n.getGateType().equals("NAND")||n.getGateType().equals("AND")) {
			return getAssignStack(n, 1, state);
		}
		else if(n.getGateType().equals("NOR")||n.getGateType().equals("OR")) {
			return getAssignStack(n, 0, state);
		}
		else return new ArrayList<Assignment>();
	}
	
	/**
	 * private function to assign all x inputs of a given node to a particular value
	 * used in D-Frontier processing while propagating the errors
	 * @param n
	 * @param val
	 * @param state
	 * @return
	 */
	private ArrayList<Assignment> getAssignStack(Node n, int val, State state){
		ArrayList<Node> fanInNodes = n.getFanInNodes();
		ArrayList<Assignment> assignStack = new ArrayList<Assignment>();
		for(int i=0; i<fanInNodes.size(); i++) {
			int index = fanInNodes.get(i).getIndex();
			if(state.getValAtIndex(index)==4) {
				assignStack.add(new Assignment(index,val));				
			}
		}
		return assignStack;
	}
}

/**
 * class used to represent an assignment
 * consists of line number and the value that should be assigned to it
 * @author Divya Nandihalli
 *
 */
class Assignment{
	private int line;
	private int value;
	public Assignment(int line, int value) {
		this.line = line;
		this.value = value;
	}
	public int getLine() {
		return line;
	}
	public int getVal() {
		return value;
	}
	public String toString() {
		return "Making assignment " + line + " to value " + value + "\n"; 
	}
}
