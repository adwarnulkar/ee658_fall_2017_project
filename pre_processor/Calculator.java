/**
 * 
 * @author Divya Nandihalli
 * This class contains the operation of and, or, xor and not for four valued logic 
 * The functions nand and nor can be developed using a combination and /or and not operation
 * These operations are used in imply and justify functions of the TestPatGenNew class
 * logic value representations using integers - 
 * 0 - logic 0
 * 1 - logic 1
 * 2 - fault D (stuck at 0)
 * 3 - fault D_bar (stuck at 1)
 * 4 - x (uninitialized/can be anything)
 */
public class Calculator {

	/**
	 * This function calculates the and operation in four valued logic
	 * @param val1 & val2 integers representing the logic values
	 * @return integer representing result of and operation
	 */
	public static int andCalc(int val1, int val2) {
		if(val1 == 0 || val2 == 0) return 0;
		if(val1 == 1) return val2;
		if(val2 == 1) return val1;
		if(val1 == val2) return val1;
		return 4;
	}

	/**
	 * This function calculates the or operation in four valued logic
	 * @param val1 & val2 integers representing the logic values
	 * @return integer representing result of or operation
	 */
	public static int orCalc(int val1, int val2) {
		if(val1 == 1 || val2 == 1) return 1;
		if(val1 == 0) return val2;
		if(val2 == 0) return val1;
		if(val1 == val2) return val1;
		return 4;
	}
	
	/**
	 * This function calculates the and operation in four valued logic
	 * @param val1 & val2 integers representing the logic values
	 * @return integer representing result of xor operation
	 */
	public static int xorCalc(int val1, int val2) {
		if(val1 == 1) return notCalc(val2);
		if(val2 == 1) return notCalc(val1);
		if(val1 == 0) return val2;
		if(val2 == 0) return val1;
		if(val1 == val2) return 0;
		if(val1 != val2) return 1;
		return 4;
	}
	
	/**
	 * This function calculates the and operation in four valued logic
	 * @param val1 & val2 integers representing the logic values
	 * @return integer representing result of not operation
	 */
	public static int notCalc(int val) {
		if(val == 4) return 4;
		if(val == 1) return 0;
		if(val == 0) return 1;
		if(val == 2) return 3;
		if(val == 3) return 2;
		return 4;
	}
}
