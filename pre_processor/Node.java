import java.util.ArrayList;
import java.util.Scanner;
/**
 * This class contains the representation of a circuit Node
 * it contains information such as fanInNodes, fanOutNodes, gatetype etc for every node
 * @author Divya Nandihalli
 *
 */
public class Node {
	
	//constants
	public static final int PRIMARY_INPUT = 0, PRIMARY_OUTPUT = 1, GATE_OUTPUT = 2; 
	public static final int INPUT = 0, BRANCH = 1, XOR = 2, OR = 3, NOR = 4, NOT = 5, NAND = 6, AND = 7; 
	
	//instance members
	private int index;
	private int nodeType;
	private int outLine;
	private int gateType;
	private int fanOut;
	private int fanIn;
	private ArrayList<Integer> inlines;
	private	ArrayList<Node> fanOutNodes;
	private ArrayList<Node> fanInNodes;
	private boolean checkPoint;
	
	/**
	 * Constructor initializes the node information
	 * @param idx
	 * @param line - String which is a line from the circuit file in "self" format
	 */
	public Node(int idx, String line) {
		index = idx;
		inlines = new ArrayList<Integer>();
		fanInNodes = new ArrayList<Node>();
		fanOutNodes = new ArrayList<Node>();
		Scanner in = new Scanner(line);
		if(in.hasNextInt()) nodeType = in.nextInt();
		if(in.hasNextInt()) outLine = in.nextInt();
		if(in.hasNextInt()) gateType = in.nextInt();
		if(in.hasNextInt()) fanOut = in.nextInt();
		if(in.hasNextInt()) fanIn = in.nextInt();
		else fanIn = 1;    //for a branch
		while(in.hasNextInt()) inlines.add(in.nextInt());
		if(gateType == BRANCH) { //different representation in case of a branch
			inlines.add(fanOut);
			fanOut = 1;
		}
		in.close();
		checkPoint = false;
	}
	
	/**
	 * checks if a given node is primary output
	 * @return
	 */
	public boolean isPO() {
		return (nodeType==3);
	}
	
	/**
	 * gets the index of the given node
	 * @return
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * get the indices of fanInNodes
	 * @return
	 */
	public ArrayList<Integer> getInLines(){
		return inlines;
	}
	
	/**
	 * add a fanInNode to the list of fanInNodes of the given node
	 * @param n
	 */
	public void addFanInNode(Node n) {
		fanInNodes.add(n);
	}
	

	/**
	 * add a fanOutNode to the list of fanOutNodes of the given node
	 * @param n
	 */
	public void addFanOutNode(Node n) {
		fanOutNodes.add(n);
	}
	
	/**
	 * set the given node as a checkpoint
	 */
	public void setCheckPoint() { 
		checkPoint = true;
	}
	
	/**
	 * get the list of fanOutNodes for a given Node
	 * @return
	 */
	public ArrayList<Node> getFanOutNodes() {
		return fanOutNodes;
	}
	
	/**
	 * get the list of fanInNodes for a given Node
	 * @return
	 */
	public ArrayList<Node> getFanInNodes() {
		return fanInNodes;
	}	
	
	/**
	 * get the fanOut of the given Node
	 * @return
	 */
	public int getFanOut() {
		return fanOutNodes.size();
	}
	
	/**
	 * check if the node is a checkpoint
	 * @return
	 */
	public boolean isCheckPoint() {
		return checkPoint;
	}
	
	/**
	 * get the gate type of the given node
	 * @return
	 */
	public String getGateType() {
		switch(gateType){
		case 0: return "PI";
		case 1: return "BRCH";
		case 2: return "XOR";
		case 3: return "OR";
		case 4: return "NOR";
		case 5: return "NOT";
		case 6: return "NAND";
		case 7: return "AND";
		default: return "unknown gate type";
		}
	}
	
	/**
	 * toString function for debugging 
	 */
	public String toString() {
		String out = "";
		out = out + outLine + " \t\t ";
		out = out + getGateType() + " \t\t ";
		for(int i=0; i<fanInNodes.size(); i++) {
			Node n = fanInNodes.get(i);
			out = out + n.getOutLine() + " ";
		}
		out = out + " \t\t ";
		for(int i=0; i<fanOutNodes.size();i++) {
			Node n = fanOutNodes.get(i);
			out = out + n.getOutLine() + " ";
		}
		if(checkPoint) out = out + " \t\t checkpoint";
		else out = out + " \t\t not checkpoint";
		out = out + "\n";
		return out;
	}
	
	/**
	 * check if any of the inputs of the nodes is not a check-point
	 * @return
	 */
	public boolean nonCheckPointInputExists() {
		
		for(int i=0; i<fanInNodes.size(); i++) {
			if(!fanInNodes.get(i).isCheckPoint()) return true; 
		}
		return false;
	}
	
	/**
	 * get the outline of a given node
	 * @return
	 */
	public int getOutLine() {
		return outLine;
	}

	
}
