import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class is the top class for Pre-processor
 * This class uses Circuit class to store information about the circuit
 * It outputs the collapsed list of faults into a file
 * the main function takes two arguments
 * @author Divya Nandihalli
 *
 */
public class PreProcessor {

	/**
	 * the main function uses two arguments
	 * the first argument is the circuit file in "self" format
	 * the second argument is the name of the faultlist file into which the faults should be written
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		String circuitFileName = "";
                String faultListFileName = "";
		
		
		if(args.length < 2) {
			System.out.println("Error: Missing input/output file");
		}
		else {
			//read the file names
			circuitFileName = args[0];
            faultListFileName = args[1];
			File file = new File(circuitFileName);
			Scanner in = new Scanner(file);
			
			//read the circuit in
			Circuit ckt = new Circuit(in);
			in.close();
			
			//get the faults and write into file
			ArrayList<Integer> faultList = ckt.getFaultList();
			writeFaultsToFile(faultList,faultListFileName);
			
			
		}
	}
	
	/**
	 * private function to write the fault list into file
	 * @param faultList
	 * @param fileName
	 * @throws IOException
	 */
	private static void writeFaultsToFile(ArrayList<Integer> faultList, String fileName) throws IOException {
		FileWriter outFile = new FileWriter(fileName);
		for(int i=0;i<faultList.size();i++) {
			String fault = "" + faultList.get(i) + "\n";
			outFile.write(fault);
		}
		outFile.close();
	}
	
	

}
