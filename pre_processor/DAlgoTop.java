import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This is the top class for the DAlgorithm
 * this contains the main class
 * @author Divya Nandihalli
 *
 */
public class DAlgoTop {

	/**
	 * the main class reads the fault list and processes each fault one at a time
	 * the argument for the main class should be circuit file, fault list file and file into which the test patterns have to be written
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("Top level class");
		String circuitFileName = "";
                String faultListFileName = "";
                String testVectorFileName = "";
		
		
		if(args.length < 3) {
			System.out.println("Error: Missing input/output file");
		}
		else {
			
			//the filenames are read from the arguments to the progran
			circuitFileName = args[0];
            faultListFileName = args[1];
            testVectorFileName = args[2];
            
            //read the circuit
			File file = new File(circuitFileName);
			Scanner in = new Scanner(file);
			Circuit ckt = new Circuit(in);
			in.close();
			
			//read the faults from file
			ArrayList<Integer> faultList = readFaultsFromFile(faultListFileName);
			
			//call the D-Algo for the given circuit and faults
			int cktSize = ckt.getCircuitNodeList().size();
			DAlgorithm dObj = new DAlgorithm(ckt);
			FileWriter testVecFile = new FileWriter(testVectorFileName);
			
			//process each fault in the fault list one at a time
			for(int i=0; i<faultList.size();i++) {
				int fault = faultList.get(i);
				int faultLine = fault/10;
				int faultType = fault%10;
				if(faultLine > cktSize) continue;
				if(faultType > 1) continue;
				//call the D-Algo for each fault
				dObj.D_top(faultLine, faultType,testVecFile);
									
			}
			testVecFile.close();
			
		}
	}
	
	/**
	 * private function to read faults from the file
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private static ArrayList<Integer> readFaultsFromFile(String fileName) throws IOException {
		ArrayList<Integer> faultList = new ArrayList<Integer>();
		File file = new File(fileName);
		Scanner in = new Scanner(file);
		while(in.hasNextInt()) {
			faultList.add(in.nextInt());			
		}
		in.close();
		return faultList;
	}

}
