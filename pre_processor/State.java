import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.Iterator;

/**
 * This class contains information of the current state of the circuit
 * it contains information of the current value assignments
 * it contains the D_frontier and J_frontier
 * it contains a boolean indicating if the error is currently at the PO
 * @author Divya Nandihalli
 *
 */
public class State {
	/*
	 * private data
	 */
	private Map<Integer,Integer> assignments;
	private boolean validity;
	private Set<Integer> D_Frontier;
	private Set<Integer> D_Frontier_tried;
	private Set<Integer> J_Frontier;
	private Set<Integer> J_Frontier_tried;
	private boolean isErrorAtPO;
	
	
	/**
	 * Constructor
	 */
	public State() {
		assignments = new TreeMap<Integer,Integer>(); 
		validity = true;
		D_Frontier = new HashSet<Integer>();
		D_Frontier_tried = new HashSet<Integer>();
		J_Frontier_tried = new HashSet<Integer>();
		J_Frontier = new HashSet<Integer>();
		isErrorAtPO = false;
	}
	
	/**
	 * Copy Constructor
	 * @param s
	 */
	public State(State s) {
		assignments = new TreeMap<Integer,Integer>(s.assignments);
		validity = s.validity;
		D_Frontier = new HashSet<Integer>(s.D_Frontier);
		D_Frontier_tried = new HashSet<Integer>(s.D_Frontier_tried);
		J_Frontier_tried = new HashSet<Integer>(s.J_Frontier_tried);
		J_Frontier = new HashSet<Integer>(s.J_Frontier);
		isErrorAtPO = s.isErrorAtPO;
	}
	
	/**
	 * makes an assignment by adding the given inputs to the assignments set
	 * @param index
	 * @param value
	 */
	public void makeAssignment(Integer index, Integer value) {
		assignments.put(index, value);
	}
	
	/**
	 * gets the current value at a given line
	 * @param index
	 * @return
	 */
	public int getValAtIndex(int index) {
		if(assignments.containsKey(index)) return assignments.get(index);
		else return 4; //represents x
	}
	
	/**
	 * checks if the D_frontier contains a line
	 * @param line
	 * @return
	 */
	public boolean containsDLine(int line) {
		return (D_Frontier.contains(line)||D_Frontier_tried.contains(line));
	}
	/**
	 * adds the line to J_frontier
	 * @param n
	 */
	public void addJLine(Node n) {
		if(J_Frontier.contains(n.getIndex())||J_Frontier.contains(n.getIndex())) return;
		J_Frontier.add(n.getIndex());
	}
	/**
	 * adds the line to D_frontier
	 * @param n
	 */
	public void addDLine(Node n) {
		if(containsDLine(n.getIndex())) return;
		D_Frontier.add(n.getIndex());
	}
	/**
	 * removes the given node from D_frontier
	 * @param n
	 */
	public void removeDLine(Node n) {
		D_Frontier.remove(n.getIndex());
	}
	/**
	 * removes the given line from D_frontier
	 * @param n
	 */
	public void removeDLine(int n) {
		D_Frontier.remove(n);
	}
	/**
	 * removes the given node from J_frontier
	 * @param n
	 */
	public void removeJLine(Node n) {
		J_Frontier.remove(n.getIndex());
	}

	/**
	 * removes the given line from J_frontier
	 * @param n
	 */
	public void removeJLine(int n) {
		J_Frontier.remove(n);
	}
	
	/**
	 * checks if current assignment is valid
	 * @return
	 */
	public boolean isValid() {
		return validity;
	}
	
	/**
	 * set the validity of assignments
	 * @param value
	 */
	public void setValidity(boolean value) {
		validity = value;
	}
	
	/**
	 * set the state error at PO
	 */
	public void setErroAtPO() {
		isErrorAtPO = true;
	}
	
	/**
	 * check if error at PO
	 * @return
	 */
	public boolean errorAtPO() {
		return isErrorAtPO;
	}
	
	/**
	 * get an untried gate from D_frontier
	 * @return
	 */
	public int getUntriedGatefromD() {
		Set<Integer> lines = D_Frontier;
		Iterator<Integer> iter = lines.iterator();
		if(iter.hasNext()) {
			int line = iter.next();
			D_Frontier_tried.add(line);
			D_Frontier.remove(line);
			return line; 
		}
		return -1;
	}
	
	/**
	 * get a line from J_frontier
	 * @return
	 */
	public int getJLine() {
		Iterator<Integer> iter = J_Frontier.iterator();
		if(!iter.hasNext()) return -1;
		int JLine = iter.next();
		return JLine;
	}
	
	/**
	 * toString for debugging
	 */
	public String toString() {
		String s = "";
		Set<Integer> lines = assignments.keySet();
		Iterator<Integer> iter = lines.iterator();
		s = s + "Current assignments\n";
		while(iter.hasNext()) {
			int line = iter.next();
			s = s + line + "\t" + assignments.get(line) + "\n"; 
		}
		
		s = s + "D Frontier";
		lines = D_Frontier;
		iter = lines.iterator();
		while(iter.hasNext()) {
			int line = iter.next();
			s = s + line + "\n";
 		}
		
		s = s + "J Frontier";
		iter = J_Frontier.iterator();
		while(iter.hasNext()) {
			int line = iter.next();
			s = s + line + "\n";
 		}
		
		if(isErrorAtPO) s = s + "Error is at PO!\n";
		else s = s + "Error is not at PO!\n";
		
		return s;
	}
	
}

/**
 * This class is used to represent a D_Line
 * it contains the line number and boolean indicating whether it has been tried
 * @author Divya Nandihalli
 *
 */
class DLine {
	private int outline;
	private boolean tried;
	public DLine(int number) {
		outline = number;
		tried = false;
	}
	public boolean is_tried() {
		return tried;
	}
	public int compareTo(DLine other) {
		return other.outline - this.outline;
	}
}
