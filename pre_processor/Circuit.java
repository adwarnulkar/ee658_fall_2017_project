import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

/**
 * This class contains the details of the circuit (circuit connections, primary inputs, primary outputs, etc)
 * The initialization is done by  reading the Scanner in 
 * The circuit is expected to be in "self" format 
 * @author Divya Nandihalli
 *
 */
public class Circuit {
	
	/**
	 * private data 
	 */
	private ArrayList<Node> cktNodeList;
	private ArrayList<Node> primaryInputs;
	private Map<Integer,Integer> outLineToIndex;
	private ArrayList<Integer> faultList;
        private Set<Integer> faultSet;
	private ArrayList<Node> checkPointLines;
	
	/**
	 * Constructor - 
	 * Reads the scanner element and forms Nodes and creates the data structure 
	 * which stores the circuit information
	 * @param in - Scanner 
	 */
	public Circuit(Scanner in) {
		
		//initialize all the data
		cktNodeList = new ArrayList<Node>();
		outLineToIndex = new TreeMap<Integer,Integer>();
		primaryInputs = new ArrayList<Node>();
		checkPointLines = new ArrayList<Node>();
		int idx = 0;
		
		//parse the input stream
		while(in.hasNextLine()) {
			String line  = in.nextLine();
			Node n = new Node(idx, line);
			cktNodeList.add(n);
			if(n.getGateType().equals("PI")) primaryInputs.add(n);				
			outLineToIndex.put(n.getOutLine(),idx);
			idx ++;			
			if(n.getGateType().equals("PI")||n.getGateType().equals("BRCH")) {
				checkPointLines.add(n);
				n.setCheckPoint();
			}
		}
		
		//calls a private method connectCircuit to parse through the list of nodes 
		//and make connections between them
		connectCircuit();
		
	}
	
	/**
	 * returns the cktNodeList data containing the ArrayList of all the nodes in the circuit
	 * @return
	 */
	public ArrayList<Node> getCircuitNodeList(){
		return cktNodeList;
	}

	/**
	 * returns the primaryInputs data containing the ArrayList of all the primary inputs of the circuit
	 * @return
	 */
	public ArrayList<Node> getPrimaryInputs(){
		return primaryInputs;
	}
	
	/**
	 * private method to make connections between the nodes read in while constructing the circuit
	 */
	private void connectCircuit() {
		
		//update fanins using the data in each Node
		for(int i=0; i<cktNodeList.size(); i++) {
			Node currNode = cktNodeList.get(i);
			ArrayList<Integer> Inlines = currNode.getInLines();
			for(int j=0; j<Inlines.size(); j++) {
				int inline = Inlines.get(j);
				Integer nidx = outLineToIndex.get(inline);
				Node inNode = cktNodeList.get(nidx);
				currNode.addFanInNode(inNode);
				inNode.addFanOutNode(currNode);
			}
		}
		
		//sort the primary inputs before storing
		Collections.sort(primaryInputs,new NodeComparator());
		
	}
	
	/**
	 * toString function to print out the circuit for Debugging
	 */
	public String toString() {
		String out = "Node \t\t Type \t\t In \t\t Out\n";
		for(int i=0; i<cktNodeList.size(); i++) {
			out = out + cktNodeList.get(i);
		}
		return out;
	}
	
	/**
	 * Uses the checkpoint theorem and fault equivalence to come up with a list of faults for the circuit
	 * All primary inputs and branches are check points
	 * CheckPointGates consists of all the gates have a checkpoint at it's input
	 * @return
	 */
	public ArrayList<Integer> getFaultList() {
		ArrayList<Node> checkPointGates = new ArrayList<Node>();
		Set<Integer> checkPointGateLines = new HashSet<Integer>();
		faultList = new ArrayList<Integer>();
		faultSet = new HashSet<Integer>();

		//get all the checkPointLines from the circuit
		for(int i=0; i<checkPointLines.size();i++) {
			Node n = checkPointLines.get(i);
			
			//if a given node is a branch, it has a single output and add this output to the checkpointgate
			if(n.getGateType().equals("BRCH")) {
				Node fanOutNode = n.getFanOutNodes().get(0);	
				if(!checkPointGateLines.contains(fanOutNode.getOutLine())) {
					checkPointGateLines.add(fanOutNode.getOutLine());
					checkPointGates.add(fanOutNode);
				}
			}
			

			if(n.getGateType().equals("PI")) {
				ArrayList<Node> fanOutNodes = n.getFanOutNodes();
				
				//if a primary input feeds a branch, add both stuck at zero and stuck at 1 faults
				if(fanOutNodes.size()>1) {
					addFault(n,0);
					addFault(n,1);
				}
				
				//else add it's output to the checkPointGates array
				else {
					Node fanOutNode = fanOutNodes.get(0);
					if(!checkPointGateLines.contains(fanOutNode.getOutLine())) {
						checkPointGateLines.add(fanOutNode.getOutLine());
						checkPointGates.add(fanOutNode);
					}
				}
			}
		}
		
		//process the checkpoint gates
		for(int i=0; i<checkPointGates.size();i++) {
			addFaultsAtNode(checkPointGates.get(i));
		}
		
		return faultList;
		
	}

	/**
	 * private function to add fault at a particular node to the faultList
	 * @param n - Node with fault
	 * @param faultType - integer representing fault (0 - SA0, 1 - SA1)
	 */
	private void addFault(Node n, int faultType) {
		int fault = n.getIndex()*10+faultType;
                if(!faultSet.contains(fault)){
                    faultList.add(fault);
                    faultSet.add(fault);
                }
	}
	
	/**
	 * private function that processes the faults at a gate output based on the type of gate
	 * @param n - Node whose faults are processed
	 */
	private void addFaultsAtNode(Node n) {
		ArrayList<Node> fanInNodes = n.getFanInNodes();
		
		//NAND, AND processing
		if(n.getGateType().equals("NAND")||n.getGateType().equals("AND")) {
			//fault equivalence (stuck-at 0 faults)
			if(n.nonCheckPointInputExists()) {
				//can ignore all stuck-at-0 faults
			}
			else {
				//choose one input and add it's stuck at 0 fault
				addFault(fanInNodes.get(0),0);
			}
			
			for(int i=0; i<fanInNodes.size();i++) {
				Node fin = fanInNodes.get(i);
				if(fin.isCheckPoint()) addFault(fin,1);
			}
		}
		
		//NOR, OR processing
		if(n.getGateType().equals("NOR")||n.getGateType().equals("OR")) {
			//fault equivalence (stuck-at 1 faults)
			if(n.nonCheckPointInputExists()) {
				//can ignore all stuck-at-1 faults
			}
			else {
				//choose one input and add it's stuck at 1 fault
				addFault(fanInNodes.get(0),1);
			}
			for(int i=0; i<fanInNodes.size();i++) {
				Node fin = fanInNodes.get(i);
				if(fin.isCheckPoint()) addFault(fin,0);
			}
		}
		

		//XOR and NOT processing
		if(n.getGateType().equals("XOR") || n.getGateType().equals("NOT")) {
			for(int i=0; i<fanInNodes.size();i++) {
				Node fin = fanInNodes.get(i);
				if(fin.isCheckPoint()) {
					addFault(fin,0);
					addFault(fin,1);
				}
			}			
		}
		
	}
}

/**
 * class used to sort the Nodes based on their index numbers
 * @author Divya Nandihalli
 *
 */
class NodeComparator implements Comparator<Node> {
	public int compare(Node a, Node b) {
		return (a.getIndex() - b.getIndex());
	}
}
