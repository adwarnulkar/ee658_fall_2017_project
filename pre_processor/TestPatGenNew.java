import java.util.ArrayList;
import java.util.Stack;

public class TestPatGenNew {
	Circuit ckt;
	int faultLine;
	int faultType; //0 - stuckAt0, 1 - stuckAt1
	Stack<ProcessEntry> justifyStack;
	Stack<Node> implyStack;
	State currState;
	
	/*
	 * constructor 
	 * inputs - circuit, faultLine and faultType
	 */
	public TestPatGenNew(Circuit ckt, int faultLine, int faultType) {
		this.ckt = ckt;
		this.faultLine = faultLine;
		this.faultType = faultType;
		this.justifyStack = new Stack<ProcessEntry>();
		this.implyStack = new Stack<Node>();
	}
	
	/*
	 * sets the state of the generator to given state
	 */
	public void setState(State state) {
		this.currState = state;
	}
	
	/*
	 * try fault assignment
	 */
	public void setFault() {
		if(tryAssignment(faultLine,faultType+2)) {} //System.out.println("Assignment successful");
		else { System.out.println("Assignment not successful!"); }
	}
	
	/*
	 * to assign the given line to given value
	 * calls justify to propagate the assignments backwards
	 * calls imply to propagate the assignments ahead
	 * returns false if either of imply or justify fails
	 */
	public boolean tryAssignment(int line, int val) {
		
		justifyStack.push(new ProcessEntry(line,val));
		
		//justify
		if(!justify()) return false;
		
		return true;
	}
	
	/*
	 * return a copy of current state
	 */
	public State getState() {
		return new State(currState);
	}
	
	/**
	 * print out the current state
	 */
	public void printState() {
		System.out.println(currState);
	}
	
	/** 
	 * get the test vector using current assignments
	 * @return String
	 */
	public String getTestVec() {
		ArrayList<Node> primaryInputs = ckt.getPrimaryInputs();
		
		String testVec = "";
		for(int i=0; i<primaryInputs.size();i++) {
			
			int val = currState.getValAtIndex(primaryInputs.get(i).getIndex());
			
			//if val is not 0 or 1, it has to be processed differently
			if(val == 4) val = 1;
			if(val == 2) val = 1;
			if(val == 3) val = 0;
			testVec = testVec + val;
		}
		testVec = testVec + "\n";
		return testVec;
	}
	
	/** 
	 * get the test vector when D-Algo failed and current assignments are invalid
	 * @return String
	 */
	public String getTestVecDummy() {
		ArrayList<Node> primaryInputs = ckt.getPrimaryInputs();
		String testVec = "";
		for(int i=0; i<primaryInputs.size();i++) {
			testVec = testVec + "-";
		}
		testVec = testVec + "\n";
		return testVec;
	}
	/*----------------------JUSTIFY functions---------------------*/
	
	/**
	 * private function justifies the values till the stack is empty
	 * @return
	 */
	private boolean justify() {
		while(!justifyStack.isEmpty()) {
			ProcessEntry pe = justifyStack.pop();
			int index = pe.getIndex();
			int value = pe.getVal();
			addOutNodesToImply(ckt.getCircuitNodeList().get(index));
			
			//make assignment
			if(value == 1 || value == 0) currState.removeDLine(index);
			if(currState.getValAtIndex(index)==value) continue;
			else if(currState.getValAtIndex(index)==4) {
				if(ckt.getCircuitNodeList().get(index).isPO() && (value == 2 || value == 3))
					currState.setErroAtPO();
				currState.makeAssignment(index, value);
			}
			else {
				return false; // collision
			}

			backPropagate(index,value);
		}
		
		//call imply once justify is complete
		if(implyStack.isEmpty()) return true;
		if(imply()) return true;
		else return false;
	}
	
	/**
	 * private function that puts the output node of given node to implyStack
	 * @param n
	 */
	private void addOutNodesToImply(Node n) {
		ArrayList<Node> OutNodes = n.getFanOutNodes();
		for(int i=0; i<OutNodes.size(); i++) {
			implyStack.push(OutNodes.get(i));
		}
	}
	
	/**
	 * back propagates the value based on the gate type
	 * @param index
	 * @param value
	 */
	private void backPropagate(int index,int value) {
		Node node = ckt.getCircuitNodeList().get(index);
		if(value==2 || value == 1) value = 1;
		else value = 0;
		if(node.getGateType().equals("BRCH")) brchJustify(node, value);
		if(node.getGateType().equals("AND")) andJustify(node,value);
		if(node.getGateType().equals("NAND")) nandJustify(node,value);
		if(node.getGateType().equals("OR")) orJustify(node,value);
		if(node.getGateType().equals("NOR")) norJustify(node,value);
		if(node.getGateType().equals("NOT")) notJustify(node,value);
		//no way to justify xor gate
		if(node.getGateType().equals("XOR")) return;
	}
	
	
	
	//justify functions (gate specific)
	//justify for a branch
	private void brchJustify(Node node, int value) {
		Node fanInNode = node.getFanInNodes().get(0);
		justifyStack.push(new ProcessEntry(fanInNode.getIndex(),value));
		
	}
	
	//justify for and 
	private void andJustify(Node node, int value) {
		switch(value) {
		case 1 : 
			setAllInputs(node,1);
			break;
		case 0 : 
			singleInputAtX(node,0);
			break;
		}
	}
	
	//justify for nand
	private void nandJustify(Node node, int value) {
		switch(value) {
		case 1 : 
			singleInputAtX(node,0);
			break;
		case 0 : 
			setAllInputs(node,1);
			break;					
		}
	}
	
	//justify for or
	private void orJustify(Node node, int value) {
		switch(value) {
		case 0 : 
			setAllInputs(node,0);
			break;
		case 1 :
			singleInputAtX(node,1);
		}
	}
	
	//justify for nor
	private void norJustify(Node node, int value) {
		switch(value) {
		case 1 : 
			setAllInputs(node,0);
			break;
		case 0 :
			singleInputAtX(node,1);		
		}
	}
	
	//justify for not
	private void notJustify(Node node, int value) {
		switch(value) {
		case 0 : 
			setAllInputs(node,1); 
			break;
		case 1 : 
			setAllInputs(node,0);
			break;
		}
	}
	
	//set all inputs of a node to given value (add to justifyStack to assign)
	private void setAllInputs(Node node, int value) {
		ArrayList<Node> fanInNodes = node.getFanInNodes();
		for(int i=0; i<fanInNodes.size(); i++) {
			Node fanInNode = fanInNodes.get(i);
			justifyStack.push(new ProcessEntry(fanInNode.getIndex(),value));
		}
	}
	
	//check if the node has a single input at x, if yes add it to justifyStack with value to be assigned
	private void singleInputAtX(Node node, int value) {
		ArrayList<Node> fanInNodes = node.getFanInNodes();
		Node singleX = fanInNodes.get(0);
		int countXNode = 0;
		for(int i=0; i<fanInNodes.size(); i++) {
			Node fanInNode = fanInNodes.get(i); 
			if(currState.getValAtIndex(fanInNode.getIndex())==4) {
				countXNode++;
				singleX = fanInNode;
			}
		}
		
		if(countXNode == 1) {
			ProcessEntry pe = new ProcessEntry(singleX.getIndex(),value);
			justifyStack.push(pe);
		}
		else {
			currState.addJLine(node);
		}
	}
	
	/*-----------------------IMPLY FUNCTIONS----------------------------------*/
	
	
	//imply called till the implyStack is empty
	private boolean imply(){
		while(!implyStack.isEmpty()) {
			Node node = implyStack.pop();
			int result = calcResultAtNode(node);
			if(result!=4) {
				int index = node.getIndex();
				int valAtIndex = currState.getValAtIndex(index);
				currState.removeJLine(index);
				currState.removeDLine(index);
				if(valAtIndex==result) continue;
				else if(valAtIndex==4) {
					if(node.isPO() && (result == 2 || result == 3)) currState.setErroAtPO();
					currState.makeAssignment(index, result); 
				}
				//dont rise any exceptions if it is just fault excitation
				else if(index==faultLine && ((result == 0 && valAtIndex == 3)||(result == 1 && valAtIndex == 2))) continue;
				else {
					return false;
				}
				addFanOutToImplyStack(node);
			}
		}
		return true;
	}
	
	/**
	 * calculates the result at node based on the node type 
	 * @param node
	 * @return
	 */
	private int calcResultAtNode(Node node) {
		ArrayList<Node> fanInNodes = node.getFanInNodes();
		if(node.getGateType().equals("BRCH")) {
			//branch has single fanIn
			return currState.getValAtIndex(node.getFanInNodes().get(0).getIndex());
		}
		else if(node.getGateType().equals("AND") || node.getGateType().equals("NAND")) {
			int result = 1;
			boolean Dfound = false;
			for(int i=0; i<fanInNodes.size();i++) {
				int val = currState.getValAtIndex(fanInNodes.get(i).getIndex());
				
				if(val == 2 || val == 3) Dfound = true;
				result = Calculator.andCalc(result,val);
				//once one input is 0, it will remain as such
				if(result == 0) break;
			}

			int currVal = currState.getValAtIndex(node.getIndex());
			//if one of the inputs is D/Dbar while output is x
			if(result == 4 && Dfound && currVal == 4) currState.addDLine(node);
			if(node.getGateType().equals("AND")) {
				//if(countX==1 && currVal==0 && result==4) justifyStack.push(new ProcessEntry(xIndex,0));
				return result;
			}
			else {
				//if(countX==1 && currVal==1 && result==4) justifyStack.push(new ProcessEntry(xIndex,0));
				return Calculator.notCalc(result);
			}
		}
		else if(node.getGateType().equals("OR")||node.getGateType().equals("NOR")) {
			int result = 0;
			boolean Dfound = false;
			for(int i=0; i<fanInNodes.size();i++) {
				int val = currState.getValAtIndex(fanInNodes.get(i).getIndex());
				
				if(val == 2 || val == 3) Dfound = true;
				result = Calculator.orCalc(result,val);
				//once one input is 0, it will remain as such
				if(result == 1) break;
			}
			int currVal = currState.getValAtIndex(node.getIndex());
			//if one of the inputs is D/Dbar while output is x
			if(result == 4 && Dfound && currVal == 4) currState.addDLine(node);
			if(node.getGateType().equals("OR")) {
				//if(countX==1 && currVal==1 && result==4) justifyStack.push(new ProcessEntry(xIndex,1));
				return result;
			}
			else {
				//if(countX==1 && currVal==0 && result==4) justifyStack.push(new ProcessEntry(xIndex,1));
				return Calculator.notCalc(result);
			}
		}
		else if(node.getGateType().equals("XOR")) {
			int result = 0;
			boolean Dfound = false;
			for(int i=0; i<fanInNodes.size();i++) {
				int val = fanInNodes.get(i).getIndex();
				if(val == 2 || val == 3) Dfound = true;
				result = Calculator.xorCalc(result,val);
			}
			//if one of the inputs is D/Dbar while output is x
			if(result == 4 && Dfound) currState.addDLine(node);
			return result;
		}
		else if(node.getGateType().equals("NOT")) {
			return Calculator.notCalc(currState.getValAtIndex(fanInNodes.get(0).getIndex()));
		}
		return 4;
	}
	
	//add fanout of the node to imply stack
	private void addFanOutToImplyStack(Node node) {
		ArrayList<Node> fanOutNodes = node.getFanOutNodes();
		for(int i=0; i<fanOutNodes.size(); i++) {
			implyStack.push(fanOutNodes.get(i));
		}
	}
	
}
//value representation-
//0 - 0, 1 - 1, 2 - D, 3 - D', 4 - x (unused)

/**
 * this class is used to represent the line and value to be assigned to the line
 * this is used in the imply and justify stacks
 * @author Divya Nandihalli
 *
 */
class ProcessEntry {
	private int index;
	private int value;
	public ProcessEntry(int index, int value) {
		this.index = index;
		this.value = value;
	}
	public int getIndex() {
		return index;
	}
	public int getVal() {
		return value;
	}
	public String toString() {
		return index + "to value " + value;
	}
}

