# # ATPG Verification
# 
# - Author : Aditya Warnulkar
# - Email : warnulka@usc.edu
# 
# The purpose of this notebook is to implement deductive fault simulation for combo logic
# 
# The approach used is as follows
# - Read the circuit file (.ckt)
# - Parse the ckt file and create a class for each node
# - Levelise the circuit
# - Initialise Fault list at the inputs
# - Propogate the fault list upto PO

# ### Import libraries
# - Numpy used for node list manipulation
# - Pandas used for displaying circuit description

import numpy as nump
import pandas as pd
import sys

# Read command line arguments
ckt_file_name = sys.argv[1];
fault_list_file_name = sys.argv[2];
input_vector_file_name = sys.argv[3];
output_file_name = sys.argv[4];

# GATE dictionary
gate_dict = dict();
gate_dict[0] = 'IPT';
gate_dict[1] = 'BRCH';
gate_dict[2] = 'XOR';
gate_dict[3] = 'OR';
gate_dict[4] = 'NOR';
gate_dict[5] = 'NOT';
gate_dict[6] = 'NAND';
gate_dict[7] = 'AND';

#print gate_dict

# Node type dictionary
node_type_dict = dict();
node_type_dict[0] = 'GATE';
node_type_dict[1] = 'PI';
node_type_dict[2] = 'BRCH';
node_type_dict[3] = 'PO';

#print node_type_dict


# Define a node class
class Node:
    '''Common Base class for all the circuit nodes'''
    
    def __init__ (self,node_type=0,node_num=0,node_idx=0,gate_type=0,value=0,num_fout=0,num_fin=0):
        self.node_type = node_type;
        self.node_num = node_num;
        self.node_idx = node_idx;
        self.gate_type = gate_type;
        self.num_fout = num_fout;
        self.num_fin = num_fin;
        self.fin_list = list();
        self.unode_list = list(); #TODO: Isn't fin_list enough ?
        self.level = -1;
        self.value = -1;
        self.c = -1;
        self.fault_list = list();


# Update up-node list
def update_unode(node_list):
    
    """This functions find the indx of all the fin's of a node and store it in unode_list"""
    
    for np in node_list:
        if(np.node_type != 1):
            np.unode_list = [node_dict[x] for x in np.fin_list];
            


# ### Print circuit description
# Below functions create a pandas dataframe for the circuit and prints it


# Print circuit description (after parsing)
def pc(node_list):
    
    """This function print the circuit description"""
    
    node_type_list = list();
    node_idx_list = list();
    node_number_list = list();
    num_fout_list = list();
    num_fin_list = list();
    node_fin_list = list();
    node_level_list = list();
    node_value_list = list();
    node_c_list = list();
    
    for np in node_list:
        
        node_idx_list.append(np.node_idx);
        num_fout_list.append(np.num_fout);
        num_fin_list.append(len(np.fin_list));
        node_number_list.append(np.node_num);
        node_level_list.append(np.level);
        node_value_list.append(np.value);
        node_c_list.append(np.c);
        
        if (np.node_type == 0): #GATE
            node_type_list.append(gate_dict[np.gate_type]);
            node_fin_list.append([','.join(map(str,np.unode_list))]);
        elif(np.node_type == 1): #PI
            node_type_list.append(gate_dict[np.gate_type]);
            node_fin_list.append(['-']);
        elif(np.node_type == 2): #BRCH
            node_type_list.append(gate_dict[np.gate_type]);
            node_fin_list.append([','.join(map(str,np.unode_list))]);
        elif(np.node_type == 3): #PO
            node_type_list.append(node_type_dict[np.node_type]);
            node_fin_list.append([','.join(map(str,np.unode_list))]);

    # Display circuit dataframe
    d = {'INDX':node_idx_list,
         'NODE_TYPE':node_type_list,
         'NODE_NUM':node_number_list,
         'NUM_FOUT':num_fout_list,
         'NUM_FIN':num_fin_list,
         'FIN_INDX':node_fin_list,
         'LEVEL':node_level_list,
         'VALUE':node_value_list,
         'C_VALUE':node_c_list}
    
    df = pd.DataFrame(data=d,columns=['INDX','NODE_TYPE','NODE_NUM','NUM_FOUT','NUM_FIN','FIN_INDX','LEVEL','VALUE','C_VALUE']);
    
    return df;
        


# ### Levelization
# The goal of this function is to assign level to each node of the circuit.
# 
# The level of the circuit will be used to perform logic simulation.


def lev(node_list):
    
    """This function accepts node_list and assigns level to each node"""
    
    node_count = len(node_list);
    #print 'Number of Nodes is {}'.format(node_count);
    
    # Assign level 0 to all PI
    num_pi = 0;
    for np in node_list:
        if(np.node_type == 1): #PI
            np.level = 0;
            num_pi = num_pi + 1;
    
    #print 'Number of PI is {}'.format(num_pi);
    
    
    ulevel_count = len(node_list) - num_pi;
    
    while(ulevel_count>0):
        for np in node_list:
            if (np.node_type != 1):
                in_level = [node_list[x].level for x in np.unode_list];
                in_level = nump.array(in_level);
                if (nump.sum(in_level==-1) == 0):
                    np.level = nump.max(in_level) + 1;
    
        ulevel_list = [node_list[x].level for x in range(0,len(node_list))];
        ulevel_list = nump.array(ulevel_list);
        ulevel_count = nump.sum(ulevel_list==-1);




# Gate operations

def XOR(inputs):
    y = inputs[0];
    for i in range(1,len(inputs)):
        y = y ^ inputs[i];
    
    return y;

def AND(inputs):
    #TODO: This can be made more efficient. No need to check all inputs if 0 occurs
    y = inputs[0];
    for i in range(1,len(inputs)):
        y = y and inputs[i];
    
    return y;

def OR(inputs):
    y = inputs[0];
    for i in range(1,len(inputs)):
        y = y or inputs[i];
    
    return y;


# ## Set functions
# The goal of section is to implement UNION , INTERSECT and MINUS functions
# 
# These functions will be used by fault evaluation algorithm to perform union, intersectio of fault lists


def UNION(lists):
    if len(lists) == 0:
        return lists;
    else:
        return list(set.union(*map(set, lists)));

def INTERSECT(lists):
    if len(lists) == 0:
        return lists;
    else:
        return list(set.intersection(*map(set, lists)));

def MINUS(list_1,list_2):
    return list(set(list_1) - set(list_2));


# ## Fault Eval functions
# 
# This section implements Fault Evaluation functions
# 
# Note that the functions are different for NOT-BRCH , XOR , other gates


# Fault list evaluation functions
def FAULT_EVAL(np,node_list):
    
    list_intersect = list();
    list_union = list();
    list_output = list();
    
    # Check all-non-control v/s at-least one control scenario
    value_list = [node_list[x].value for x in np.unode_list];
    value_list = nump.array(value_list);
    c_count = sum(value_list == np.c);

    # Find fault at output
    if (np.value == 1):
        list_output.append(10*np.node_idx);
    else:
        list_output.append(10*np.node_idx + 1);

    # Apply fault list set formula
    if(c_count > 0):
        for x in np.unode_list:
            if(node_list[x].value == np.c):
                list_intersect.append(node_list[x].fault_list);
            else:
                list_union.append(node_list[x].fault_list);

        # Evaluate fault list at the node
        #debug: print 'INTERSECT is {}'.format(list_intersect);
        #debug: print 'UNION is {}'.format(list_union);
        #debug: print 'LIST_OUT is {}'.format(list_output);
        np.fault_list = UNION([MINUS(INTERSECT(list_intersect),UNION(list_union)) , list_output]);
    else: # All non controlling
        for x in np.unode_list:
            list_union.append(node_list[x].fault_list);

        # Evaluate fault list at the node
        list_union.append(list_output);
        #debug: print 'UNION is {}'.format(list_union);
        #debug: print 'LIST_OUT is {}'.format(list_output);
        np.fault_list = UNION(list_union);



def FAULT_EVAL_NOT_BRCH(np,node_list):
    
    lists = list();
    
    for x in np.unode_list:
        lists.append(node_list[x].fault_list);

    if(np.value == 1):
        lists.append([10*np.node_idx]);
    else:
        lists.append([10*np.node_idx + 1]);

    # Find output fault list
    np.fault_list = UNION(lists);


    
def FAULT_EVAL_XOR(np,node_list):
    
    lists = list();
    list_output = list();
    
    # Find fault at output
    if (np.value == 1):
        list_output.append(10*np.node_idx);
    else:
        list_output.append(10*np.node_idx + 1);

    # Evaluate fault list
    for x in np.unode_list:
        lists.append(node_list[x].fault_list);

    #debug: print 'LISTS is {}'.format(lists);
    #debug: print 'LIST_OUTPUT is {}'.format(list_output);
    np.fault_list = UNION([MINUS(UNION(lists),INTERSECT(lists)) , list_output]);


# ### simple_logic_simulation


def deductive_fault_simulation(node_list,in_vector):
    
    """This function implements simple_logic_simulation"""
    
    #TODO: Error checking if length of vector != number of PIs
    
    # Make all the fault lists empty
    for np in node_list:
        np.fault_list = list();
    
    # Assign values to all primary inputs
    count = 0;
    for np in node_list:
        if (np.node_type == 1): #PI
            #debug:print 'BEFORE fault list {}'.format(np.fault_list);
            np.value = in_vector[count];
            if(np.value == 1):
                #np.fault_list.append(10*np.node_idx);
                np.fault_list = [10*np.node_idx];
            else:
                #np.fault_list.append(10*np.node_idx + 1);
                np.fault_list = [10*np.node_idx + 1];
            count = count + 1;
            #debug:print 'INPUT IDX {} list is {}'.format(np.node_idx,np.fault_list);
    
    max_level = max([node_list[x].level for x in range(0,len(node_list))]); #TODO: Why repeat this for each vector?
    
    for l in range(1,max_level+1):
        for np in node_list:

            if(np.level == l):
                if(np.gate_type == 1): #BRCH
                    
                    # Evaluate node value
                    np.value = node_list[np.unode_list[0]].value;
                    
                    # Evaluate fault list
                    FAULT_EVAL_NOT_BRCH(np,node_list);
                
                elif(np.gate_type == 2): #XOR
                    
                    # Evaluate node value
                    inputs = [node_list[x].value for x in np.unode_list];
                    np.value = XOR(inputs);
                    
                    # Evaluate fault list
                    FAULT_EVAL_XOR(np,node_list);
                
                elif(np.gate_type == 3): #OR
                    
                    # Evaluate node value
                    inputs = [node_list[x].value for x in np.unode_list];
                    np.value = OR(inputs);
                    
                    # Evaluate fault list
                    FAULT_EVAL(np,node_list);
                
                elif(np.gate_type == 4): #NOR
                    
                    # Evaluate node value
                    inputs = [node_list[x].value for x in np.unode_list];
                    np.value = int(not (OR(inputs)));
                    
                    # Evaluate fault list
                    FAULT_EVAL(np,node_list);
                
                elif(np.gate_type == 5): #NOT
                    
                    # Evaluare node value
                    np.value = int (not ( node_list[np.unode_list[0]].value ));
                    
                    # Evaluate fault list
                    FAULT_EVAL_NOT_BRCH(np,node_list);
                
                elif(np.gate_type == 6): #NAND
                    
                    # Evaluate node value
                    inputs = [node_list[x].value for x in np.unode_list];
                    np.value = int(not (AND(inputs)));
                    
                    # Evaluate fault list
                    FAULT_EVAL(np,node_list);
                
                elif(np.gate_type == 7): #AND
                    
                    # Evaluate node value
                    inputs = [node_list[x].value for x in np.unode_list];
                    np.value = AND(inputs);
                    
                    # Evaluate fault list
                    FAULT_EVAL(np,node_list);



# ### Creating node list (Parser)


def parser(ckt_file_name):
    
    ckt_file = open(ckt_file_name,'r');
    node_list = [];
    count = 0;
    node_dict = {}; # Mapping of num -> idx

    for line in ckt_file:

        line_list = line.split();
        line_list = map(int,line_list);

        # Unpack node attributes
        node_type = line_list[0];
        node_num  = line_list[1];
        gate_type = line_list[2];
        num_fout  = line_list[3];

        # Map node_num to idx
        node_dict[node_num] = count;

        # Create node instance
        node = Node(node_type=node_type,node_num=node_num,node_idx=count,gate_type=gate_type,num_fout=num_fout);

        #node.fin_list = list(); #TODO : can this be done in constructor ??

        if ((node_type == 0) or (node_type == 3)) : #GATE or PO
            node.num_fin = line_list[4];
            for x in range(0,node.num_fin):
                node.fin_list.append(line_list[x+5]);
        elif (node_type == 2): # BRCH
            node.fin_list.append(line_list[3]); #TODO: for BRCH num_fout exist . Does it mean something ?
        elif (node_type == 1): #PI
            node.num_fin = 0;

        # Assign controling values (TODO: Combine the below conditions)
        if(gate_type == 3): #OR
            node.c = 1;
        elif(gate_type == 4): #NOR
            node.c = 1
        elif(gate_type == 6): #NAND
            node.c = 0;
        elif(gate_type == 7): #AND
            node.c = 0;

        # Push the node object into node_list array
        node_list.append(node);
        count = count + 1;
    
    return (node_list,node_dict);




# Parse the ckt file
(node_list,node_dict) = parser(ckt_file_name);



# Update unodes
update_unode(node_list);



# Perform levelization
lev(node_list);


# ### Print the node details


df = pc(node_list);
#print(df.to_string())


# ## Perform deductive fault simulation (MAIN)


input_vector_file = open(input_vector_file_name,'r');
fault_list_file = open(fault_list_file_name,'r');
out_file = open(output_file_name,'w');

for line in input_vector_file:

    fault = fault_list_file.readline();
    fault = fault.rstrip('\n');
    fault = fault.split();
    
    # Initialise fault
    fault = map(int,fault);
    fault = fault[0];
    print('Processing Fault : {}'.format(fault));
    out_file.write('Fault : {} ;; '.format(fault));
    
    line = line.rstrip('\n');
    line = list(line);
    in_vector = map(int,line);
    
    # Assign vector values to PI
    deductive_fault_simulation(node_list,in_vector);
    
    #Display output fault list
    out_file.write('Vector : {} ;; '.format(''.join(line)));
    fault_list_full = [];
    for np in node_list:
        if (np.node_type == 3):
            
            fault_list = np.fault_list;
            fault_list.sort();
            
            fault_list_full.extend(np.fault_list);

             
    #out_file.write('Fault list : {} ;; '.format(fault_list_full));
    #out_file.write('\n');

    fault_list_full = nump.array(fault_list_full);
    if(nump.sum(fault_list_full==fault) > 0):
        out_file.write('FAULT_DETECTED\n');
    else:
        out_file.write('FAULT_NOT_DETECTED\n');


input_vector_file.close();
out_file.close();


